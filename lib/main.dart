import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_smartlook/flutter_smartlook.dart';

// import 'package:flutter_uxcam/flutter_uxcam.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:merchant_app/models/attributes_model.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:merchant_app/models/cart_model.dart';
import 'package:merchant_app/models/config_model.dart';
import 'package:merchant_app/models/image_model.dart';
import 'package:merchant_app/models/line_model.dart';
import 'package:merchant_app/models/location_model.dart';
import 'package:merchant_app/models/merchant_model.dart';
import 'package:merchant_app/models/product_model.dart';
import 'package:merchant_app/models/restaurant_model.dart';
import 'package:statusbarz/statusbarz.dart';

// import 'constants/app_constant.dart';
import 'constants/config_constant.dart';
import 'i18n/translation_service.dart';
import 'routes/app_pages.dart';



// import 'services/tracking_service.dart';
import 'utils/error_capture_util.dart';
import 'values/style.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(ConfigModelAdapter());
  Hive.registerAdapter(RestaurantModelAdapter());
  Hive.registerAdapter(CartModelAdapter());
  Hive.registerAdapter(ProductModelAdapter());
  Hive.registerAdapter(ImageModelAdapter());
  Hive.registerAdapter(MerchantModelAdapter());
  Hive.registerAdapter(LineModelAdapter());
  Hive.registerAdapter(LocationModelAdapter());
  Hive.registerAdapter(AttributeModelAdapter());
  await Hive.openBox<CartModel>(BOX_CART);
  await Hive.openBox<ConfigModel>(BOX_CONFIG);
  await Hive.openBox<RestaurantModel>(BOX_USER);

  runApp(
    OverlaySupport(
      child: StatusbarzCapturer(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(Get.context!);

            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus!.unfocus();
            }
          },
          child: GetMaterialApp(
              title: 'EPIS Cloud - Mon Mi Pos',
              theme: Style.lightTheme,
              darkTheme: Style.lightTheme,
              themeMode: ThemeMode.dark,
              debugShowCheckedModeBanner: false,
              defaultTransition: Transition.rightToLeft,
              locale: TranslationService.locale,
              fallbackLocale: TranslationService.fallbackLocale,
              translations: TranslationService(),
              unknownRoute: AppPages.routes[0],
              initialRoute: AppPages.INITIAL,
              getPages: AppPages.routes,
              navigatorObservers: kDebugMode
                  ? [
                Statusbarz.instance.observer,
              ]
                  : [
                Statusbarz.instance.observer,
              ],
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('vi'),
              ],
              onInit: () async {
                // if (!kDebugMode) {
                //   SetupOptions options =
                //       (SetupOptionsBuilder(SMART_LOOK_API_KEY)
                //             ..Fps = 2
                //             ..StartNewSession = true)
                //           .build();
                //   Smartlook.setupAndStartRecording(options);
                //
                //   // calling all functions to make sure nothing crashes
                //   Smartlook.setEventTrackingMode(
                //       EventTrackingMode.FULL_TRACKING);
                //   Smartlook.enableCrashlytics(true);
                // }

                // await DynamicLinkService().retrieveDynamicLink();
              },
              onReady: () {

              },
              onDispose: () async {
                await Hive.box(BOX_USER).close();
                await Hive.box(BOX_CONFIG).close();
                await Hive.close();
              }),
        ),
      ),
    ),
  );
}
