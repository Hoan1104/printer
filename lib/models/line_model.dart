import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/product_model.dart';
part 'line_model.g.dart';
@HiveType(typeId: 8)
@JsonSerializable(explicitToJson: true)
class LineModel extends HiveObject {
  @HiveField(0, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int quantity;

  @HiveField(1)
  ProductModel? product;

  LineModel({this.quantity = 0, this.product});

  factory LineModel.fromJson(Map<String, dynamic> json) =>
      _$LineModelFromJson(json);

  Map<String, dynamic> toJson() => _$LineModelToJson(this);
}