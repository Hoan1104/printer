import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/base_model.dart';
part 'image_model.g.dart';
@HiveType(typeId: 5)
@JsonSerializable(explicitToJson: true)
class ImageModel extends HiveObject {
  @HiveField(0, defaultValue: '')
  @JsonKey(defaultValue: '')
  String path;

  @HiveField(1, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(2, defaultValue: '')
  @JsonKey(defaultValue: '')
  String id;


  ImageModel({this.path = '', this.name= '', this.id = ''});

  factory ImageModel.fromJson(Map<String, dynamic> json) =>
      _$ImageModelFromJson(json);

  Map<String, dynamic> toJson() => _$ImageModelToJson(this);
}