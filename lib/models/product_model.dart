import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/attributes_model.dart';
import 'package:merchant_app/models/base_model.dart';
import 'package:merchant_app/models/image_model.dart';
import 'package:merchant_app/models/merchant_model.dart';
part 'product_model.g.dart';
@HiveType(typeId: 4)
@JsonSerializable(explicitToJson: true)
class ProductModel extends HiveObject {
  @HiveField(0, defaultValue: '')
  @JsonKey(defaultValue: '')
  String slug;

  @HiveField(1, defaultValue: '')
  @JsonKey(defaultValue: '')
  String merchant_id;

  @HiveField(2, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  double price;

  @HiveField(3, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int status;

  @HiveField(4, defaultValue: '')
  @JsonKey(defaultValue: '')
  String description;

  @HiveField(5, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(6, defaultValue:  <ImageModel>[])
  @JsonKey(defaultValue: <ImageModel>[])
  List<ImageModel> images;

  @HiveField(7)
  MerchantModel? merchant;

  @HiveField(8)
  ImageModel? image;

  @HiveField(9, defaultValue: '')
  @JsonKey(defaultValue: '')
  String id;

  @HiveField(10, defaultValue:  <AttributeModel>[])
  @JsonKey(defaultValue: <AttributeModel>[])
  List<AttributeModel> attributes;

  ProductModel(
      {this.slug = '',
      this.merchant_id = '',
      this.price = 0,
      this.status = 0,
      this.description = '',
      this.name = '',
      this.images = const <ImageModel>[],
      this.attributes = const <AttributeModel>[],
      this.merchant,
      this.image,
      this.id = ''});

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
