// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merchant_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MerchantModelAdapter extends TypeAdapter<MerchantModel> {
  @override
  final int typeId = 6;

  @override
  MerchantModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MerchantModel(
      tag: (fields[0] as List?)?.cast<int>(),
      is_open: fields[1] == null ? false : fields[1] as bool,
      status: fields[2] == null ? 0 : fields[2] as int,
      is_verified: fields[3] == null ? 0 : fields[3] as int,
      description: fields[4] == null ? '' : fields[4] as String,
      phone: fields[5] == null ? '' : fields[5] as String,
      email: fields[6] == null ? '' : fields[6] as String,
      name: fields[7] == null ? '' : fields[7] as String,
      code: fields[8] == null ? 0 : fields[8] as int,
      merchant_id: fields[9] == null ? '' : fields[9] as String,
      image: fields[10] == null ? '' : fields[10] as String,
      slug: fields[13] == null ? '' : fields[13] as String,
      id: fields[11] == null ? '' : fields[11] as String,
      location: fields[12] as LocationModel?,
    );
  }

  @override
  void write(BinaryWriter writer, MerchantModel obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.tag)
      ..writeByte(1)
      ..write(obj.is_open)
      ..writeByte(2)
      ..write(obj.status)
      ..writeByte(3)
      ..write(obj.is_verified)
      ..writeByte(4)
      ..write(obj.description)
      ..writeByte(5)
      ..write(obj.phone)
      ..writeByte(6)
      ..write(obj.email)
      ..writeByte(7)
      ..write(obj.name)
      ..writeByte(8)
      ..write(obj.code)
      ..writeByte(9)
      ..write(obj.merchant_id)
      ..writeByte(10)
      ..write(obj.image)
      ..writeByte(11)
      ..write(obj.id)
      ..writeByte(12)
      ..write(obj.location)
      ..writeByte(13)
      ..write(obj.slug);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MerchantModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MerchantModel _$MerchantModelFromJson(Map<String, dynamic> json) =>
    MerchantModel(
      tag: (json['tag'] as List<dynamic>?)?.map((e) => e as int).toList(),
      is_open: json['is_open'] as bool? ?? false,
      status: json['status'] as int? ?? 0,
      is_verified: json['is_verified'] as int? ?? 0,
      description: json['description'] as String? ?? '',
      phone: json['phone'] as String? ?? '',
      email: json['email'] as String? ?? '',
      name: json['name'] as String? ?? '',
      code: json['code'] as int? ?? 0,
      merchant_id: json['merchant_id'] as String? ?? '',
      image: json['image'] as String? ?? '',
      slug: json['slug'] as String? ?? '',
      id: json['id'] as String? ?? '',
      location: json['location'] == null
          ? null
          : LocationModel.fromJson(json['location'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MerchantModelToJson(MerchantModel instance) =>
    <String, dynamic>{
      'tag': instance.tag,
      'is_open': instance.is_open,
      'status': instance.status,
      'is_verified': instance.is_verified,
      'description': instance.description,
      'phone': instance.phone,
      'email': instance.email,
      'name': instance.name,
      'code': instance.code,
      'merchant_id': instance.merchant_id,
      'image': instance.image,
      'id': instance.id,
      'location': instance.location?.toJson(),
      'slug': instance.slug,
    };
