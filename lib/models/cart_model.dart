
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/line_model.dart';
import 'package:merchant_app/models/product_model.dart';

part 'cart_model.g.dart';

@HiveType(typeId: 2)
@JsonSerializable(explicitToJson: true)
class CartModel extends HiveObject {
  @HiveField(0, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(1)
  List<LineModel> line;

  CartModel(this.name, this.line);

  factory CartModel.fromJson(Map<String, dynamic> json) =>
      _$CartModelFromJson(json);

  Map<String, dynamic> toJson() => _$CartModelToJson(this);
}
