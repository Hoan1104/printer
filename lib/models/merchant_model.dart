import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/base_model.dart';
import 'package:merchant_app/models/location_model.dart';

part 'merchant_model.g.dart';
@HiveType(typeId: 6)
@JsonSerializable(explicitToJson: true)
class MerchantModel extends HiveObject {
  @HiveField(0)
  List<int>? tag;

  @HiveField(1, defaultValue: false)
  @JsonKey(defaultValue: false)
  bool is_open;

  @HiveField(2, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int status;

  @HiveField(3, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int is_verified;

  @HiveField(4, defaultValue: '')
  @JsonKey(defaultValue: '')
  String description;

  @HiveField(5, defaultValue: '')
  @JsonKey(defaultValue: '')
  String phone;

  @HiveField(6, defaultValue: '')
  @JsonKey(defaultValue: '')
  String email;

  @HiveField(7, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(8, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int code;

  @HiveField(9, defaultValue: '')
  @JsonKey(defaultValue: '')
  String merchant_id;

  @HiveField(10, defaultValue: '')
  @JsonKey(defaultValue: '')
  String image;

  @HiveField(11, defaultValue: '')
  @JsonKey(defaultValue: '')
  String id;

  @HiveField(12)
  LocationModel? location;

  @HiveField(13, defaultValue: '')
  @JsonKey(defaultValue: '')
  String slug;

  MerchantModel(
      {this.tag,
      this.is_open = true,
      this.status = 0,
      this.is_verified = 0,
      this.description = '',
      this.phone = '',
      this.email = '',
      this.name = '',
      this.code = 0,
      this.merchant_id = '',
      this.image = '',
      this.slug = '',
      this.id = '',
      this.location});

  factory MerchantModel.fromJson(Map<String, dynamic> json) =>
      _$MerchantModelFromJson(json);

  Map<String, dynamic> toJson() => _$MerchantModelToJson(this);
}
