import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/base_model.dart';
part 'location_model.g.dart';
@HiveType(typeId: 7)
@JsonSerializable(explicitToJson: true)
class LocationModel extends HiveObject {
  @HiveField(0,defaultValue: '')
  @JsonKey(defaultValue: '')
  String address;

  @HiveField(1,defaultValue: '')
  @JsonKey(defaultValue: '')
  String province_name;

  @HiveField(2,defaultValue: '')
  @JsonKey(defaultValue: '')
  String district_name;

  @HiveField(3,defaultValue: '')
  @JsonKey(defaultValue: '')
  String ward_name;

  @HiveField(4,defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int ward_id;

  @HiveField(5,defaultValue:0)
  @JsonKey(defaultValue: 0)
  int district_id;

  @HiveField(6,defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int province_id;

  @HiveField(7)
  DateTime? created_at;

  @HiveField(8)
  DateTime? updated_at;


  LocationModel(
      {this.address = '',
      this.province_name = '',
      this.district_name = '',
      this.ward_name = '',
      this.ward_id = 0,
      this.district_id = 0,
      this.province_id = 0,
      this.created_at,
      this.updated_at});


  factory LocationModel.fromJson(Map<String, dynamic> json) =>
      _$LocationModelFromJson(json);

  Map<String, dynamic> toJson() => _$LocationModelToJson(this);
}
