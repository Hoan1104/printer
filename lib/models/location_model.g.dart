// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LocationModelAdapter extends TypeAdapter<LocationModel> {
  @override
  final int typeId = 7;

  @override
  LocationModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LocationModel(
      address: fields[0] == null ? '' : fields[0] as String,
      province_name: fields[1] == null ? '' : fields[1] as String,
      district_name: fields[2] == null ? '' : fields[2] as String,
      ward_name: fields[3] == null ? '' : fields[3] as String,
      ward_id: fields[4] == null ? 0 : fields[4] as int,
      district_id: fields[5] == null ? 0 : fields[5] as int,
      province_id: fields[6] == null ? 0 : fields[6] as int,
      created_at: fields[7] as DateTime?,
      updated_at: fields[8] as DateTime?,
    );
  }

  @override
  void write(BinaryWriter writer, LocationModel obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.province_name)
      ..writeByte(2)
      ..write(obj.district_name)
      ..writeByte(3)
      ..write(obj.ward_name)
      ..writeByte(4)
      ..write(obj.ward_id)
      ..writeByte(5)
      ..write(obj.district_id)
      ..writeByte(6)
      ..write(obj.province_id)
      ..writeByte(7)
      ..write(obj.created_at)
      ..writeByte(8)
      ..write(obj.updated_at);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocationModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocationModel _$LocationModelFromJson(Map<String, dynamic> json) =>
    LocationModel(
      address: json['address'] as String? ?? '',
      province_name: json['province_name'] as String? ?? '',
      district_name: json['district_name'] as String? ?? '',
      ward_name: json['ward_name'] as String? ?? '',
      ward_id: json['ward_id'] as int? ?? 0,
      district_id: json['district_id'] as int? ?? 0,
      province_id: json['province_id'] as int? ?? 0,
      created_at: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updated_at: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
    );

Map<String, dynamic> _$LocationModelToJson(LocationModel instance) =>
    <String, dynamic>{
      'address': instance.address,
      'province_name': instance.province_name,
      'district_name': instance.district_name,
      'ward_name': instance.ward_name,
      'ward_id': instance.ward_id,
      'district_id': instance.district_id,
      'province_id': instance.province_id,
      'created_at': instance.created_at?.toIso8601String(),
      'updated_at': instance.updated_at?.toIso8601String(),
    };
