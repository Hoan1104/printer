import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
part 'restaurant_model.g.dart';

@HiveType(typeId: 1)
@JsonSerializable(explicitToJson: true)
class RestaurantModel extends HiveObject {
  @HiveField(1, defaultValue: '')
  @JsonKey(defaultValue: '')
  String description;

  @HiveField(2, defaultValue: '')
  @JsonKey(defaultValue: '')
  String phone;

  @HiveField(3, defaultValue: '')
  @JsonKey(defaultValue: '')
  String email;

  @HiveField(4, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(5, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  int code;

  @HiveField(6, defaultValue: '')
  @JsonKey(defaultValue: '')
  String merchant_id;

  @HiveField(7, defaultValue: '')
  @JsonKey(defaultValue: '')
  String id;

  RestaurantModel(
      {this.description = '',
      this.phone = '',
      this.email = '',
      this.name = '',
      this.code = 0,
      this.merchant_id = '',
      this.id = ''});

  factory RestaurantModel.fromJson(Map<String, dynamic> json) =>
      _$RestaurantModelFromJson(json);

  Map<String, dynamic> toJson() => _$RestaurantModelToJson(this);
}
