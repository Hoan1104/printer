import 'package:json_annotation/json_annotation.dart';
import 'package:merchant_app/models/base_model.dart';
import 'package:merchant_app/models/image_model.dart';

part 'category_model.g.dart';

@JsonSerializable(explicitToJson: true)
class CategoryModel extends BaseModel {
  @JsonKey(defaultValue: '')
  String slug;

  @JsonKey(defaultValue: '')
  String merchant_id;

  @JsonKey(defaultValue: 0)
  int status;

  @JsonKey(defaultValue: '')
  String description;

  @JsonKey(defaultValue: '')
  String name;

  @JsonKey(defaultValue: '')
  String id;

  ImageModel? image;

  @JsonKey(defaultValue: <ImageModel>[])
  List<ImageModel> images;

  CategoryModel(
      {this.slug = '',
      this.merchant_id = '',
      this.status = 0,
      this.description = '',
      this.name = '',
      this.id = '',
      this.images = const <ImageModel>[],
      this.image});

  factory CategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryModelToJson(this);
}
