// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) =>
    CategoryModel(
      slug: json['slug'] as String? ?? '',
      merchant_id: json['merchant_id'] as String? ?? '',
      status: json['status'] as int? ?? 0,
      description: json['description'] as String? ?? '',
      name: json['name'] as String? ?? '',
      id: json['id'] as String? ?? '',
      images: (json['images'] as List<dynamic>?)
              ?.map((e) => ImageModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
      image: json['image'] == null
          ? null
          : ImageModel.fromJson(json['image'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'merchant_id': instance.merchant_id,
      'status': instance.status,
      'description': instance.description,
      'name': instance.name,
      'id': instance.id,
      'image': instance.image?.toJson(),
      'images': instance.images.map((e) => e.toJson()).toList(),
    };
