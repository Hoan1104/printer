import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
part 'attributes_model.g.dart';


@HiveType(typeId: 9)

@JsonSerializable(explicitToJson: true)
class AttributeModel extends HiveObject {
  @HiveField(0, defaultValue: '')
  @JsonKey(defaultValue: '')
  String product_id;

  @HiveField(1, defaultValue: 0)
  @JsonKey(defaultValue: 0)
  double price;

  @HiveField(2, defaultValue: '')
  @JsonKey(defaultValue: '')
  String name;

  @HiveField(3, defaultValue: '')
  @JsonKey(defaultValue: '')
  String id;

  AttributeModel(
      {this.product_id = '', this.price = 0, this.name = '', this.id = ''});


  factory AttributeModel.fromJson(Map<String, dynamic> json) =>
      _$AttributeModelFromJson(json);

  Map<String, dynamic> toJson() => _$AttributeModelToJson(this);

}
