class ItemMenu {
  final int id;
  final String imgUrl;
  final String title;
  final double priceItem;
  final String description;

  ItemMenu(
      {required this.id,
      required this.imgUrl,
      required this.title,
      required this.priceItem,
      required this.description});
}

List<ItemMenu> demoItems = [
  ItemMenu(
      id: 1,
      imgUrl:
          "https://images.unsplash.com/photo-1538688525198-9b88f6f53126?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
      title: "Coffee Capuchino",
      priceItem: 40000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 2,
      imgUrl:
          "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      title: "Espresso or Short Black",
      priceItem: 48000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 3,
      imgUrl:
          "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      title: "Long Black or Americano",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 4,
      imgUrl:
          "https://images.unsplash.com/photo-1513161455079-7dc1de15ef3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Americano",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 5,
      imgUrl:
          "https://images.unsplash.com/photo-1544457070-4cd773b4d71e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=843&q=80",
      title: "Espresso",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 5,
      imgUrl:
          "https://images.unsplash.com/photo-1532323544230-7191fd51bc1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Short Black",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 7,
      imgUrl:
          "https://images.unsplash.com/photo-1513161455079-7dc1de15ef3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Capuchino",
      priceItem: 46000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 8,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Ristretto",
      priceItem: 72000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 9,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Capuchino",
      priceItem: 52000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 10,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Café Latte",
      priceItem: 48000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 11,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Flat White",
      priceItem: 62000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
];

List<ItemMenu> demoItems2 = [
  ItemMenu(
      id: 12,
      imgUrl:
          "https://images.unsplash.com/photo-1538688525198-9b88f6f53126?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
      title: "Coffee Capuchino",
      priceItem: 40000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 13,
      imgUrl:
          "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      title: "Espresso or Short Black",
      priceItem: 48000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 14,
      imgUrl:
          "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      title: "Long Black or Americano",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 15,
      imgUrl:
          "https://images.unsplash.com/photo-1513161455079-7dc1de15ef3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Americano",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 15,
      imgUrl:
          "https://images.unsplash.com/photo-1544457070-4cd773b4d71e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=843&q=80",
      title: "Espresso",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 18,
      imgUrl:
          "https://images.unsplash.com/photo-1532323544230-7191fd51bc1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Short Black",
      priceItem: 42000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 16,
      imgUrl:
          "https://images.unsplash.com/photo-1513161455079-7dc1de15ef3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
      title: "Capuchino",
      priceItem: 46000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 17,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Ristretto",
      priceItem: 72000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 19,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Capuchino",
      priceItem: 52000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 20,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Café Latte",
      priceItem: 48000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
  ItemMenu(
      id: 21,
      imgUrl: "https://coffee.alexflipnote.dev/random",
      title: "Flat White",
      priceItem: 62000,
      description:
          "Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans"),
];
