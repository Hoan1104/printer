// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProductModelAdapter extends TypeAdapter<ProductModel> {
  @override
  final int typeId = 4;

  @override
  ProductModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProductModel(
      slug: fields[0] == null ? '' : fields[0] as String,
      merchant_id: fields[1] == null ? '' : fields[1] as String,
      price: fields[2] == null ? 0 : fields[2] as double,
      status: fields[3] == null ? 0 : fields[3] as int,
      description: fields[4] == null ? '' : fields[4] as String,
      name: fields[5] == null ? '' : fields[5] as String,
      images: fields[6] == null ? [] : (fields[6] as List).cast<ImageModel>(),
      attributes:
          fields[10] == null ? [] : (fields[10] as List).cast<AttributeModel>(),
      merchant: fields[7] as MerchantModel?,
      image: fields[8] as ImageModel?,
      id: fields[9] == null ? '' : fields[9] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ProductModel obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.slug)
      ..writeByte(1)
      ..write(obj.merchant_id)
      ..writeByte(2)
      ..write(obj.price)
      ..writeByte(3)
      ..write(obj.status)
      ..writeByte(4)
      ..write(obj.description)
      ..writeByte(5)
      ..write(obj.name)
      ..writeByte(6)
      ..write(obj.images)
      ..writeByte(7)
      ..write(obj.merchant)
      ..writeByte(8)
      ..write(obj.image)
      ..writeByte(9)
      ..write(obj.id)
      ..writeByte(10)
      ..write(obj.attributes);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) => ProductModel(
      slug: json['slug'] as String? ?? '',
      merchant_id: json['merchant_id'] as String? ?? '',
      price: (json['price'] as num?)?.toDouble() ?? 0,
      status: json['status'] as int? ?? 0,
      description: json['description'] as String? ?? '',
      name: json['name'] as String? ?? '',
      images: (json['images'] as List<dynamic>?)
              ?.map((e) => ImageModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
      attributes: (json['attributes'] as List<dynamic>?)
              ?.map((e) => AttributeModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
      merchant: json['merchant'] == null
          ? null
          : MerchantModel.fromJson(json['merchant'] as Map<String, dynamic>),
      image: json['image'] == null
          ? null
          : ImageModel.fromJson(json['image'] as Map<String, dynamic>),
      id: json['id'] as String? ?? '',
    );

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'merchant_id': instance.merchant_id,
      'price': instance.price,
      'status': instance.status,
      'description': instance.description,
      'name': instance.name,
      'images': instance.images.map((e) => e.toJson()).toList(),
      'merchant': instance.merchant?.toJson(),
      'image': instance.image?.toJson(),
      'id': instance.id,
      'attributes': instance.attributes.map((e) => e.toJson()).toList(),
    };
