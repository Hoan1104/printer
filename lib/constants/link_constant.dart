const String LINK_FACEBOOK_FAN_PAGE_IOS = 'fb://profile/106561591144596';
const String LINK_FACEBOOK_FAN_PAGE_ANDROID = 'https://www.facebook.com/App.HiTrade';
const String LINK_FACEBOOK_GROUP = 'https://www.facebook.com/groups/530235621535553';
const String LINK_CHAT_FAN_PAGE = 'http://m.me/App.HiTrade';
const String LINK_WEBSITE_merchant_app = 'https://merchant_app.com/';
const String LINK_PRIVACY_POLICY = 'https://merchant_app.com/about/policy';
const String LINK_TERM_OF_USE = 'https://merchant_app.com/about/terms';
const String LINK_PAIRS = 'https://hitrade.vn/product';
const String LINK_CONTACT = 'https://hitrade.vn/product';
const String LINK_MARKET = 'https://hitrade.vn/market';
const String LINK_FORECAST_DETAIL = 'https://hitrade.vn/forecast/';
const String LINK_BLOG = 'https://hitrade.vn/blog';
const String LINK_TUTORIAL =
    'https://hitrade.vn/huong-dan-giao-dich-hang-hoa-cung-hitrade';
const String LINK_HOP_DONG_1 =
    'https://drive.google.com/file/d/19gbKHCFyHhNOARQjAmhTlFolCfmrlh7K/view?usp=sharing';
const String LINK_HOP_DONG_2 =
    'https://drive.google.com/file/d/1FZqzDsgZCZ6uDTE093InCG-u3DjN1uPt/view?usp=sharing';
const String LINK_HOP_DONG_3 =
    'https://drive.google.com/file/d/1nrHXYLcEoerMfK-TiFH1RyjaYmuRv06T/view?usp=sharing';
const String LINK_HOP_DONG_4 =
    'https://drive.google.com/file/d/1_e_Ro-aTVCMy3O4EOfEJoeh_cKHXHSFd/view?usp=sharing';
const String LINK_HOP_DONG_5 =
    'https://drive.google.com/file/d/1KKGu32ap9eTlhq7sxSyuf26RSFHWWkgH/view?usp=sharing';
const String LINK_POLICY_HI_MARGIN = 'https://hitrade.vn/about/terms';
const String LINK_POLICY_SUBSCRIPTION =
    'https://docs.google.com/document/d/1Exo7BmswlG7xtCTfLskhAxlGmPu3rK9JQYolIxtyVq4/edit';
const String LINK_BLOG_NEWS = 'https://hitrade.vn/news';
const String LINK_BLOG_FORECAST = 'https://hitrade.vn/forecase';
const String LINK_BLOG_ANNOUNCEMENT = 'https://hitrade.vn/announcement';
const String LINK_BLOG_ACADEMY = 'https://hitrade.vn/academy';
const String LINK_BLOG_HELP = 'https://merchant_app.com/help';
const String LINK_POLICY_HI_SAVING = 'https://merchant_app.com/about/terms';
const String LINK_ABOUT_merchant_app = 'https://merchant_app.com/about';
const String LINK_NEWS = 'https://merchant_app.com/';
const String LINK_REFERRAL_PROGRAM = 'https://hitrade.vn/help/r/chuong-trinh-gioi-thieu-hitrade';
const String LINK_TOKEN_INFO = 'https://hitrade.vn/su-kien-gui-tien-nhan-lien-hitrade-token-htt';
const String LINK_NFT_FORM = 'https://forms.gle/ov3mMfUC73YawMNB6';
const String LINK_BSC_SCAN_ADDRESS = 'https://testnet.bscscan.com/address/';
const String LINK_BSC_SCAN_TX = 'https://testnet.bscscan.com/tx/';