const String TYPE_IMAGE = 'image';
const String TYPE_VIDEO = 'video';
const String TYPE_AUDIO = 'audio';

const String TYPE_AUCTION = 'auction';
const String TYPE_SALE = 'sale';

const int NFT_NAME_MAX_LENGTH = 80;
const int NFT_DESC_MAX_LENGTH = 500;
const int NFT_PROPERTIES_MAX_LENGTH = 20;
const int NFT_SECRET_MAX_LENGTH = 90;

const int COLLECTION_NAME_MAX_LENGTH = 80;
const int COLLECTION_DESC_MAX_LENGTH = 500;
const int COLLECTION_SYMBOL_MAX_LENGTH = 10;

const double PRICE_UNIT_AUCTION = 0.05;

const String CATEGORY_ART = 'art';
const String CATEGORY_GAME = 'game';
const String CATEGORY_INFLUENCER = 'influencer';
const List<String> LIST_CATEGORY = [
  CATEGORY_ART,
  CATEGORY_GAME,
  CATEGORY_INFLUENCER
];

const String PARAM_FOR_OWNER = 'owner_id';
const String PARAM_FOR_CREATOR = 'creator_id';
