
class ParseUtil {
  static int parseInt(dynamic s) {
    if (s is double) {
      return s.toInt();
    }

    if (s is String) {
      if (s.isEmpty) {
        return 0;
      }

      s = s.replaceAll(',', '');

      return int.parse(s);
    }

    if (s is int) {
      return s;
    }

    return 0;
  }

  static double parseDouble(dynamic s) {
    if (s is double) {
      return s;
    }

    if (s is String) {
      if (s.isEmpty) {
        return 0.0;
      }

      s = s.replaceAll(',', '');

      return double.parse(s);
    }

    if (s is int) {
      return s + .0;
    }

    return 0;
  }


}
