import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/config_db.dart';
import 'package:merchant_app/routes/app_pages.dart';
import 'package:merchant_app/utils/dialog_util.dart';
import 'package:merchant_app/utils/object_util.dart';

class ErrorUtil {
  static ErrorUtil instance = ErrorUtil();

  static String? getError(error, trace) {
    String? errorMsg;
    if (error is DioError) {
      if (kDebugMode) {
        Get.log(
            'Error api: ${error.requestOptions.method} ${error.requestOptions.path}');
        Get.log('Error request headers: ${error.requestOptions.headers}');
        Get.log('Error request data: ${error.requestOptions.data}');
        Get.log('Error response: ${error.response?.data}');
      }

      if (error.response?.statusCode == 500 ||
          error.response?.statusCode == 501 ||
          error.response?.statusCode == 502 ||
          error.response?.statusCode == 503) {
        return 'SYSTEM_ERROR';
      }

      if (error.response?.data is String) {
        errorMsg =
            '${error.requestOptions.path}\n${error.response?.data?.toString() ?? ''}';
      } else {
        if (error.response?.data != null &&
            error.response?.data['message'] != null) {
          errorMsg = error.response?.data['message'][0];
          if (errorMsg == 'INVALID_TOKEN' ||
              errorMsg == 'YOUR_SESSION_HAS_BEEN_BLOCKED_BY_SYSTEM' ||
              errorMsg == 'YOUR_ACCOUNT_HAS_BEEN_BLOCKED_BY_SYSTEM') {
            ConfigDB().deleteConfigByName(CONFIG_ACCESS_TOKEN);
            Get.offAllNamed(Routes.ON_BOARDING_AUTH);
          }
        } else {
          if (error.response?.data == null) {
            errorMsg = 'SERVER_NOT_RESPONDING';
          } else {
            errorMsg = error.requestOptions.path + '\n' + error.toString();
          }
        }
      }
    } else {
      errorMsg = error.toString();
      if (kDebugMode) {
        Get.log(errorMsg, isError: true);
        Get.log(trace.toString(), isError: true);
      }
    }

    return errorMsg;
  }

  static FutureOr<void> catchError(error, trace) {
    if (kDebugMode) {
      Get.log(error.toString(), isError: true);
      Get.log(trace.toString(), isError: true);
    }
    String? msg = ErrorUtil.getError(error, trace);
    if (ObjectUtil.isNotEmpty(msg)) {
      DialogUtil.showErrorMessage(msg!.tr);
    } else {
      DialogUtil.showErrorMessage(error.toString());
    }

    return Future.value(null);
  }
}
