import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/custom/animation/custom_animation_toast.dart';
import 'package:merchant_app/widgets/dialog/_error_toast.dart';
import 'package:merchant_app/widgets/dialog/_icon_toast_widget.dart';
import 'package:merchant_app/widgets/dialog/_success_toast.dart';


class DialogUtil {
  static DialogUtil instance = DialogUtil();

  DialogUtil();

  static showUploadingDialog(BuildContext context) {
    Get.generalDialog(
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Center(
              child: Opacity(
                opacity: a1.value,
                child: Material(
                  color: Style.transparent,
                  child: Center(
                    child: SizedBox(
                      child: Lottie.asset(
                        AppSetting.lottieUploading,
                        width: 200,
                        height: 200,
                        fit: BoxFit.cover,
                        repeat: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierLabel: '',
        barrierDismissible: false,
        pageBuilder: (context, animation1, animation2) => const SizedBox());
  }

  static showProgressDialog(BuildContext context) {
    showDialog<dynamic>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return const Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
                height: 30.0,
                width: 30.0,
              ),
            ),
          );
        });
  }

  static showToastMessage(String description,
      {VoidCallback? onDismiss, bool isTop = true, int duration = 5}) {
    showToast(
      description,
      context: Get.overlayContext,
      textStyle: TextStyle(
        fontFamily: Style.fontMedium,
        fontSize: 14,
        color: Style.textWhiteColor,
      ),
      toastHorizontalMargin: 30,
      animation: StyledToastAnimation.slideFromTop,
      reverseAnimation: StyledToastAnimation.slideToTop,
      position: StyledToastPosition.top,
      startOffset: const Offset(0.0, -3.0),
      reverseEndOffset: const Offset(0.0, -3.0),
      duration: Duration(seconds: duration),
      //Animation duration   animDuration * 2 <= duration
      animDuration: const Duration(seconds: 1),
      curve: Curves.elasticOut,
      reverseCurve: Curves.fastOutSlowIn,
      onDismiss: onDismiss,
    );
  }

  static showSuccessMessage(String description,
      {VoidCallback? onDismiss, bool isTop = true, int duration = 5}) {
    showToastWidget(
      IconToastWidget.success(msg: description),
      context: Get.overlayContext,
      position: isTop ? StyledToastPosition.top : StyledToastPosition.bottom,
      animation: StyledToastAnimation.slideFromTop,
      reverseAnimation: StyledToastAnimation.slideToTop,
      startOffset: const Offset(0.0, -3.0),
      reverseEndOffset: const Offset(0.0, -3.0),
      duration: Duration(seconds: duration),
      animDuration: const Duration(seconds: 1),
      curve: Curves.elasticOut,
      reverseCurve: Curves.fastOutSlowIn,
      onDismiss: onDismiss,
    );
  }

  static showErrorMessage(String description,
      {VoidCallback? onDismiss, bool isTop = true, int duration = 5}) {
    showToastWidget(
      IconToastWidget.fail(msg: description),
      context: Get.overlayContext,
      position: isTop ? StyledToastPosition.top : StyledToastPosition.bottom,
      animation: StyledToastAnimation.slideFromTop,
      reverseAnimation: StyledToastAnimation.slideToTop,
      startOffset: const Offset(0.0, -3.0),
      reverseEndOffset: const Offset(0.0, -3.0),
      duration: Duration(seconds: duration),
      animDuration: const Duration(seconds: 1),
      curve: Curves.elasticOut,
      reverseCurve: Curves.fastOutSlowIn,
      onDismiss: onDismiss,
    );
  }

  static showWarningMessage(String description,
      {VoidCallback? onDismiss, bool isTop = true, int duration = 5}) {
    showToastWidget(
      IconToastWidget.warning(msg: description),
      context: Get.overlayContext,
      position: isTop ? StyledToastPosition.top : StyledToastPosition.bottom,
      animation: StyledToastAnimation.slideFromTop,
      reverseAnimation: StyledToastAnimation.slideToTop,
      startOffset: const Offset(0.0, -3.0),
      reverseEndOffset: const Offset(0.0, -3.0),
      duration: Duration(seconds: duration),
      animDuration: const Duration(seconds: 1),
      curve: Curves.elasticOut,
      reverseCurve: Curves.fastOutSlowIn,
      onDismiss: onDismiss,
    );
  }

  static showSuccessToast(String description) {
    showOverlay((context, t) {
      return CustomAnimationToast(value: t, content: SuccessToast(description));
    }, key: const ValueKey('successfully'), curve: Curves.decelerate);
  }

  static void showErrorToast(String description) {
    showOverlay((context, t) {
      return CustomAnimationToast(value: t, content: ErrorToast(description));
    }, key: const ValueKey('error'), curve: Curves.decelerate);
  }



  
}
