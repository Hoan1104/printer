import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/config_db.dart';

class BaseClient {
  static const _apiHostReal = 'https://monmi-gateway.myepis.cloud';
  static const _apiGetWay= 'https://monmi-gateway.myepis.cloud/api/v1';

  // static const _apiHostReal = 'https://nft-staging-api-v2.merchant_app.com/';
  // static const _apiHostReal = 'https://api.merchant_app.com/';
  static const _apiLookup = 'https://www.iplocate.io/api/lookup';

  // static var _apiHostCurrent = _apiHostReal;
  static const _authServiceBaseUrl = 'api/v1';
  static BaseClient? _instance;
  Dio? _dio;

  BaseClient();

  static get instance {
    _instance ??= BaseClient._internal();
    return _instance;
  }

  get apiLookup => _apiLookup;

  get apiHostReal => _apiHostReal;
  get apiGetWay => _apiGetWay;

  get authServiceBaseUrl => _apiHostReal + _authServiceBaseUrl;


  get dio => _dio;

  BaseClient._internal() {
    _dio = Dio();
    _dio!.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      options.receiveDataWhenStatusError = true;
      if (options.path.contains('/_upload_service/nft-file')) {
        options.connectTimeout = 600000;
        options.receiveTimeout = 600000;
      } else {
        options.connectTimeout = 30000;
        options.receiveTimeout = 30000;
      }

      if (kDebugMode) {
        debugPrint(
            'Call api: ${options.method} ${options.path} with request data ${options.data}');
      }
      String? token = ConfigDB().getConfigByName(CONFIG_ACCESS_TOKEN);
      // String? ipAddress = ConfigDB().getConfigByName(CONFIG_IP_ADDRESS);
      // String? location = ConfigDB().getConfigByName(CONFIG_LOCATION);

      if (token != null) {
        if (kDebugMode) {
          debugPrint('Request token: ${token.toString()}');
        }
        options.headers["Authorization"] = 'Bearer ' + token.toString();
      }


      // if (ipAddress != null) {
      //   options.headers["ip"] = ipAddress.toString();
      // }
      //
      // if (location != null) {
      //   options.headers["location"] = location.toString();
      // }

      options.headers["X-MONMI-API"] = 123;
      // Do something before request is sent
      return handler.next(options); //continue
      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onResponse: (response, handler) {
      // print(response.toString());
      // Do something with response data
      return handler.next(response); // continue
      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onError: (DioError e, handler) {
      // print(e.toString());
      // Do something with response error
      return handler.next(e); //continue
      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    }));
  }
}
