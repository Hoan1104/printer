import 'package:dio/dio.dart';
import 'package:merchant_app/repositories/base_client.dart';

class AuthenticateClient extends BaseClient {
  final client = BaseClient.instance.dio;

  Future<Response> getLocation(String? location) async {
    return client.get('$apiGetWay/locations/search?search=$location');
  }

  Future<Response> login(String keyword, String password) async {
    return client.post('$apiHostReal/api/v1/auth/login',
        data: {'account': keyword, 'password': password});
  }

  Future<Response> regester(
      String keyword, String phone, String name, String password) async {
    return client.post('https://be-dev.app.gwu.vn/api/v1/auth/register', data: {
      'email': keyword,
      'phone': phone,
      'name': name,
      'password': password
    });
  }

  Future<Response> getMe() async {
    return client.get('$apiHostReal/api/v1/auth/me');
  }

  Future<Response> getMerchant(String? address) async {
    return client
        .post('$apiGetWay/merchants/filters', data: {'search': address});
  }

  Future<Response> getMerchantBySlug(String? slug) async {
    return client.get('$apiGetWay/merchants/$slug');
  }

  Future<Response> getGlobalCategory() async {
    return client.get('$apiGetWay/categories?is_global=true');
  }

  Future<Response> getProduct(String? id) async {
    return client.get('$apiHostReal/api/v1/merchants/$id/products');
  }
  Future<Response> getCategory(String? id) async {
    return client.get('$apiHostReal/api/v1/merchants/$id/categories');
  }
}
