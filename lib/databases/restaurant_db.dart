import 'package:hive/hive.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/models/restaurant_model.dart';

class RestaurantDB {
  RestaurantModel? currentUser() {
    final boxUser = Hive.box<RestaurantModel>(BOX_USER);
    return boxUser.get(0);
  }

  Future<RestaurantModel> save(RestaurantModel userModel) async {
    final boxUser = Hive.box<RestaurantModel>(BOX_USER);
    await boxUser.put(0, userModel);

    return boxUser.get(0)!;
  }

  Future<void> clear() async {
    final boxUser = Hive.box<RestaurantModel>(BOX_USER);
    await boxUser.delete(0);
    await boxUser.clear();
  }
}
