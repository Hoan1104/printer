import 'package:hive/hive.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/models/cart_model.dart';
import 'package:merchant_app/models/line_model.dart';

class CartDB {
  List<LineModel>? getConfigByName(String name) {
    final carConfig = Hive.box<CartModel>(BOX_CART);
    return carConfig.get(name)?.line;
  }

  Future<CartModel?> save(CartModel configModel) async {
    final carConfig = Hive.box<CartModel>(BOX_CART);
    await carConfig.put(configModel.name, configModel);

    return carConfig.get(configModel.name);
  }

  Future<void> deleteConfigByName(String name) async {
    final carConfig = Hive.box<CartModel>(BOX_CART);
    await carConfig.delete(name);
  }
}
