part of 'app_pages.dart';

abstract class Routes {
  static const UNKNOWN_ROUTE = '/404';
  static const SPLASH = '/splash';
  static const INTRODUCTION = '/introduction';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const VERIFY_LOGIN_OTP = '/login/verify-email-otp';
  static const ON_BOARDING_AUTH = '/on-boarding-auth';
  static const REGISTER = '/register';
  static const VERIFY_REGISTER_OTP = '/register/verify-email-otp';
  static const DETAIL_DENI = '/DETAIL_DENI';
  static const DETAIL_RESTAURANT = '/detail_restaurant';
  static const SEARCH = '/search';
  static const FORGOT_PASS = '/FORGOT_PASS';
  static const ONBOARD = '/ONBOARD';
  static const ORDER_TRACKING = '/ORDER_TRACKING';
  static const ORDER_CHECKOUT = '/ORDER_CHECKOUT';
  static const CART = '/CART_SCREEN';
  static const YOUR_LOCATION = '/your_location';
  static const HISTORY_LOCATION = '/HISTORY_LOCATION';
  static const DETAIL_CATELORY = '/DETAIL_CATELORY';
}
