import 'package:get/get.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/config_db.dart';

import 'package:merchant_app/screens/login/binding/login_binding.dart';
import 'package:merchant_app/screens/login/view/login.dart';
import 'package:merchant_app/screens/menu_screen/main_screen.dart';
import 'package:merchant_app/screens/menu_screen/binding/menu_binding.dart';
import 'package:merchant_app/screens/menu_screen/menu_screen.dart';
import 'package:merchant_app/screens/splash/view/splash.dart';

part 'app_routes.dart';

class AppPages {

  static const INITIAL =  Routes.SPLASH;
  static const UNKNOWN_ROUTE = Routes.UNKNOWN_ROUTE;


  static final routes = [
    GetPage(name: Routes.SPLASH, page: () => SplashPage()),
    GetPage(
        name: Routes.LOGIN, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(
        name: Routes.HOME, page: () => ScreenOne(), binding: MenuBinding()),
  ];
}
