import 'package:merchant_app/components/bill_Item.dart';
import 'package:flutter/material.dart';

class BillItemProvider with ChangeNotifier {
  List<BillItem> _listBill = [];
  double _total = 0;

  //billItemProvider.getListBill()

  getListBill() => _listBill;
  getTotal() => _total;

  calculateTotal(listItems) {
    _total = 0;
    for (var i = 0; i < listItems.length; i++) {
      _total += listItems[i].priceItem * listItems[i].countItems;
    }
  }

  addItemToBill(BillItem bill) {
    // BillItem temp =
    //     _listBill.where((element) => element.billTitle == bill.billTitle).first();
    int indexItem =
        _listBill.indexWhere((item) => item.billTitle == bill.billTitle);
    if (indexItem > -1) {
      _listBill[indexItem].countItems += 1;
    } else {
      _listBill.add(bill);
    }
    calculateTotal(_listBill);
    notifyListeners();
  }

  removeItemOnBill(BillItem bill) {
    int indexItem =
        _listBill.indexWhere((item) => item.billTitle == bill.billTitle);
    if (indexItem > -1) {
      if (bill.countItems > 0) {
        _listBill[indexItem].countItems--;
      } else {
        _listBill.removeAt(indexItem);
      }
    }

    calculateTotal(_listBill);
  }
}
