import 'package:flutter/material.dart';

class ItemSelectedProvider with ChangeNotifier {
  int _currentItemIdSelected = 0;

  getCurrentItemIdSelected() => _currentItemIdSelected;

  setCurrentItemIdSelected(int id) {
    _currentItemIdSelected = id;
    notifyListeners();
  }
}
