import 'dart:async';
import 'package:get/get.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/config_db.dart';
import 'package:merchant_app/routes/app_pages.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/utils/object_util.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    config();
    super.onInit();
  }

  config() async {
    String? token = ConfigDB().getConfigByName(CONFIG_ACCESS_TOKEN);
    if (ObjectUtil.isNotEmpty(token)) {
      await Future.delayed(const Duration(milliseconds: 1500));
      Get.offNamed(Routes.HOME, preventDuplicates: false);
      Future.delayed(const Duration(milliseconds: 3000), () {
        try {
          Get.find<MenuController>();
        } catch (ex) {
        Get.offNamed(Routes.HOME, preventDuplicates: false);
        }
      });
    } else {
      // timer = Timer.periodic(Duration(seconds: 20), (t) {
      await Future.delayed(const Duration(milliseconds: 3000));
      Get.offNamed(Routes.LOGIN, preventDuplicates: false);
      // });
    }
  }
}
