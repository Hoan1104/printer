import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:merchant_app/screens/splash/controller/splash_controller.dart';
import 'package:merchant_app/values/setting.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    FlutterError.onError =
        (FlutterErrorDetails details, {bool forceReport = false}) {
      FlutterError.dumpErrorToConsole(details, forceReport: true);
    };

    return GetBuilder<SplashController>(
        init: SplashController(),
        builder: (splashController) {
          return Material(
            color: context.theme.backgroundColor,
            child: SafeArea(
              bottom: false,
              child: Scaffold(
                backgroundColor: context.theme.backgroundColor,
                body: Container(
                  alignment: Alignment.center,
                  child: Lottie.asset(
                    AppSetting.lottieSpalsh,
                    width: 300,
                    height: 300,
                    fit: BoxFit.scaleDown,
                    repeat: true,
                  ),
                ),
              ),
            ),
          );
        });
  }
}
