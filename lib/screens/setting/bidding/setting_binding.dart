import 'package:get/get.dart';
import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/screens/setting/controller/setting_controller.dart';

class SettingBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SettingController(client: AuthenticateClient()));
  }
}
