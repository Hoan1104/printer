import 'package:flutter/cupertino.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:get/get.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/cart_db.dart';
import 'package:merchant_app/databases/config_db.dart';
import 'package:merchant_app/databases/restaurant_db.dart';
import 'package:merchant_app/models/config_model.dart';
import 'package:merchant_app/models/restaurant_model.dart';
import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/routes/app_pages.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/utils/error_util.dart';
import 'package:merchant_app/utils/object_util.dart';
import 'package:merchant_app/utils/parse_util.dart';

class SettingController extends GetxController {
  final AuthenticateClient client;

  SettingController({required this.client});

  final EasyRefreshController controller = EasyRefreshController();
  final restaurant = RestaurantModel().obs;
  final TextEditingController vatController = TextEditingController();
  final TextEditingController ipController = TextEditingController();
  final TextEditingController portController = TextEditingController();
  final FocusNode vatFocus = FocusNode();
  final FocusNode ipFocus = FocusNode();
  final FocusNode portFocus = FocusNode();

  @override
  void onInit() {
    String? vatValue = ConfigDB().getConfigByName(CONFIG_VAT);
    if (ObjectUtil.isNotEmpty(vatValue)) {
      vatController.text = vatValue!;
    } else {
      vatController.text = 0.toString();
    }
    String? ipValue = ConfigDB().getConfigByName(CONFIG_IP);
    if (ObjectUtil.isNotEmpty(ipValue)) {
      ipController.text = ipValue!;
    } else {
      ipController.clear();
    }
    String? portValue = ConfigDB().getConfigByName(CONFIG_PORT);
    if (ObjectUtil.isNotEmpty(portValue)) {
      portController.text = portValue!;
    } else {
      portController.clear();
    }
    init();
    super.onInit();
  }

  init() {
    restaurant.value = RestaurantDB().currentUser()!;
    onRefresh();
  }

  Future<void> onRefresh() async {
    await client.getMe().then((response) async {
      await RestaurantDB()
          .save(RestaurantModel.fromJson(response.data['data']));
      restaurant.value = RestaurantDB().currentUser()!;
    }).catchError((error, trace) {
      ErrorUtil.catchError(error, trace);
    });
    controller.resetLoadState();
    controller.finishRefresh();
  }

  handleLogout() async {
    Get.offAllNamed(Routes.LOGIN);
    await ConfigDB().deleteConfigByName(CONFIG_USERNAME);
    await CartDB().deleteConfigByName(CONFIG_LIST_CART);
    await ConfigDB().deleteConfigByName(CONFIG_ACCESS_TOKEN);
    await ConfigDB().deleteConfigByName(CONFIG_REGISTER_USERNAME);
    await ConfigDB().deleteConfigByName(CONFIG_YOUR_ADRESS);
    await ConfigDB().deleteConfigByName(CONFIG_VAT);
  }

 updateSetting() async {
    await ConfigDB().save(ConfigModel(CONFIG_VAT, vatController.text));
    await ConfigDB().save(ConfigModel(CONFIG_IP, ipController.text));
    await ConfigDB().save(ConfigModel(CONFIG_PORT, portController.text));
    final menu = Get.find<MenuController>();
    menu.vat.value = vatController.text;

 }
}
