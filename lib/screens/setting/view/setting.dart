import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:merchant_app/screens/setting/controller/setting_controller.dart';
import 'package:merchant_app/screens/setting/view/item.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/_radius_button.dart';
import 'package:merchant_app/widgets/_title_default_text_field.dart';
import 'package:merchant_app/widgets/custom/custom_easy_refresh.dart';

class SettingPage extends GetView<SettingController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: CustomEasyRefresh(
          controller: controller.controller,
          onRefresh: controller.onRefresh,
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 10,
                    color: context.theme.textTheme.overline!.color,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Thông tin',
                      style: Style().subtitleStyle1,
                    ),
                  ),
                  buildContent(context),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Cài đặt',
                      style: Style().subtitleStyle1,
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  buildSettingVAT(context),
                  Container(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Cài đặt máy in',
                      style: Style().subtitleStyle1,
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  buildSettingPrinter(context),
                  Container(
                    height: 40,
                  ),
                  buildLogout(context),
                ],
              );
            },
            childCount: 1,
          ),
        ),
      ),
    );
  }

  Widget buildLogout(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: RadiusButton(
            isFullWidth: true,
            radius: 10,
            maxWidth: Get.width / 2,
            outsidePadding:
                EdgeInsets.only(top: 13, bottom: 20, right: 16, left: 16),
            innerPadding:
                EdgeInsets.only(top: 14, bottom: 14, left: 20, right: 20),
            indicatorSize: 24,
            text: 'Đăng xuất'.tr,
            fontFamily: Style.fontDemiBold,
            fontSize: 17,
            backgroundColor: context.theme.primaryColorDark,
            textColor: context.theme.backgroundColor,
            onTap: () => controller.handleLogout(),
          ),
        ),
        Expanded(
          child: RadiusButton(
            isFullWidth: true,
            maxWidth: Get.width / 2,
            radius: 10,
            outsidePadding:
                EdgeInsets.only(top: 13, bottom: 20, right: 16, left: 16),
            innerPadding:
                EdgeInsets.only(top: 14, bottom: 14, left: 20, right: 20),
            indicatorSize: 24,
            text: 'Cập nhật'.tr,
            fontFamily: Style.fontDemiBold,
            fontSize: 17,
            backgroundColor: context.theme.primaryColor,
            textColor: context.textTheme.subtitle2!.color,
            onTap: () => controller.updateSetting(),
          ),
        ),
      ],
    );
  }

  Widget buildContent(BuildContext context) {
    final userModel = controller.restaurant.value;

    return Column(
      children: [
        SizedBox(height: 30),
        Row(
          children: [
            Expanded(
              child: ContentItem(
                title: 'Email',
                subtitle: userModel.email,
              ),
            ),
            Expanded(
              child: ContentItem(
                title: 'Tên nhà hàng',
                subtitle: userModel.name,
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: ContentItem(
                title: 'Số điện thoại',
                subtitle: userModel.phone,
              ),
            ),
            Expanded(
              child: ContentItem(
                title: 'Mô tả',
                subtitle: userModel.description,
              ),
            ),
          ],
        ),

        // ContentItem(
        //   title: 'address',
        //   subtitle: '${userModel.address?.address}, ${userModel.address?.city}',
        // ),
        SizedBox(height: 20),
        // buildLogout(),
      ],
    );
  }

  Widget buildSettingVAT(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TitleDefaultTextField(
            radius: 10,
            controller: controller.vatController,
            focusNode: controller.vatFocus,
            title: 'Thuế VAT *'.tr,
            hintText: 'Nhập % thuê'.tr,
            textInputType: TextInputType.number,
            innerPadding:
                EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 20),
            outsidePadding: EdgeInsets.only(bottom: 10, left: 16, right: 16),
            suffixText: '%',
            suffixStyle: Style().hintStyle,
            onChanged: (text) {
              // controller.onChangeCopyright(text);
            },
          ),
        ),
        Expanded(child: SizedBox())
      ],
    );
  }

  Widget buildSettingPrinter(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TitleDefaultTextField(
            radius: 10,
            controller: controller.ipController,
            focusNode: controller.ipFocus,
            title: 'Địa chỉ IP *'.tr,
            hintText: 'vd 192.12.12'.tr,
            textInputType: TextInputType.number,
            innerPadding:
                EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 20),
            outsidePadding: EdgeInsets.only(bottom: 10, left: 16, right: 16),
            suffixText: '%',
            suffixStyle: Style().hintStyle,
            onChanged: (text) {
              // controller.onChangeCopyright(text);
            },
          ),
        ),
        Expanded(
          child: TitleDefaultTextField(
            radius: 10,
            controller: controller.portController,
            focusNode: controller.portFocus,
            title: 'Port'.tr,
            hintText: 'Vd 1200'.tr,
            textInputType: TextInputType.number,
            innerPadding:
                EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 20),
            outsidePadding: EdgeInsets.only(bottom: 10, left: 16, right: 16),
            suffixStyle: Style().hintStyle,
            onChanged: (text) {
              // controller.onChangeCopyright(text);
            },
          ),
        ),
      ],
    );
  }
}
