import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant_app/values/style.dart';


class ContentItem extends StatelessWidget {
  final String? title;
  final String? subtitle;
  final GestureTapCallback? onTap;

  const ContentItem({Key? key,
    this.title,
    this.subtitle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 18),
      child: ListTile(
        title: Text(
          title!.tr,
          style: TextStyle(
            fontFamily: Style.fontDemiBold,
            fontSize: 15,
            color: context.textTheme.caption!.color,
          ),
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              color: context.theme.cardColor,
              borderRadius: BorderRadius.circular(10)),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
          child: Text(subtitle!.tr, style: Style().subtitleStyle1),
        ),
        onTap: onTap,
      ),
    );
  }
}
