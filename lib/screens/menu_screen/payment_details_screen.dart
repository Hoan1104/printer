import 'package:get/get.dart';
import 'package:merchant_app/components/bill_Item.dart';
import 'package:merchant_app/components/payment_method.dart';
import 'package:merchant_app/models/line_model.dart';
import 'package:merchant_app/provider/bill_item.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/size_config.dart';
import 'package:flutter/material.dart';
import 'package:merchant_app/utils/app_util.dart';
import 'package:merchant_app/utils/parse_util.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/_cache.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';

class PaymentDetailsScreen extends GetView<MenuController> {
//
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(color: Colors.white),
      width: payment_Details_Screen,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          cashierAvatar(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Menu", textAlign: TextAlign.left, style: headingStyle)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Obx(() => billContent()),
          Obx(() => payment(context))
        ],
      ),
    );
  }

  Widget cashierAvatar() {
    return Container(
        height: 100,
        width: payment_Details_Screen,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 50.0,
              height: 50.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        "https://monmi.vn/static/images/moni.png")),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Nhà hàng",
                      style: descriptionStyle,
                    ),
                    Text(
                      controller.restaurant.value.name,
                      style: const TextStyle(
                        height: 1.8,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                )),
          ],
        ));
  }

  Widget billContent() {
    return Expanded(
        child: ListView.builder(
            itemCount: controller.listCart.length,
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                  key: Key(controller.listCart[index].product?.id ?? ''),
                  direction: DismissDirection.endToStart,
                  onDismissed: (direction) {
                    // controller.removeItemOnBill(controller.listBill[index]);
                  },
                  background: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(0),
                      )),
                  child: buildItem(context, controller.listCart[index]));
            }));

    //height: MediaQuery.of(context).size.height - 140,
  }

  Widget buildItem(BuildContext context, LineModel cart) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFFF5F6F9),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: CachedImage(
                  cart.product?.image?.path ?? '',
                  defaultUrl: AppSetting.imgDefault,
                  fit: BoxFit.fill,
                  radius: 16,
                  width: 88,
                  height: 88,
                ),
              ),
            ),
          ),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                cart.product?.name ?? '',
                style: TextStyle(color: Colors.black, fontSize: 16),
                maxLines: 2,
              ),
              SizedBox(height: 10),
              Text.rich(
                TextSpan(
                  text: AppUtil.formatMoney(cart.product?.price ?? 0),
                  style: Style().primaryStyle,
                  children: [
                    TextSpan(
                        text: "  x ${cart.quantity}",
                        style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      controller.removeItemOnBill(cart);
                    },
                    child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                          color: Get.context!.theme.cardColor,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 6),
                              blurRadius: 10,
                              color: Color(0xFFB0B0B0).withOpacity(0.2),
                            ),
                          ],
                        ),
                        child: Icon(Icons.remove)),
                  ),
                  SizedBox(width: 10),
                  Text('${cart.quantity}'),
                  SizedBox(width: 10),
                  GestureDetector(
                    onTap: () {
                      LineModel tempBill =
                          new LineModel(quantity: 1, product: cart.product);
                      controller.addItemToBill(tempBill);
                    },
                    child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                          color: Get.context!.theme.cardColor,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 6),
                              blurRadius: 10,
                              color: Color(0xFFB0B0B0).withOpacity(0.2),
                            ),
                          ],
                        ),
                        child: Icon(Icons.add)),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  // Widget total() {
  //   // BillItemProvider billItemProvider =
  //   //     Provider.of<BillItemProvider>(context, listen: true);
  //   double totalNoVAT = billItemProvider.getTotal();
  //   double VAT = totalNoVAT * 10 / 100;
  //   return Padding(
  //     padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
  //     child: Column(
  //       children: [
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: [
  //             Text(
  //               "SubTotal",
  //               style: titleStyle,
  //             ),
  //             Text(
  //               totalNoVAT.toString(),
  //               style: titleStyle,
  //             )
  //           ],
  //         ),
  //         SizedBox(
  //           height: 10,
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: [
  //             Text(
  //               "Tax (10%)",
  //               style: descriptionStyle,
  //             ),
  //             Text(
  //               VAT.toString(),
  //               style: descriptionStyle,
  //             )
  //           ],
  //         ),
  //         SizedBox(
  //           height: 15,
  //         ),
  //         Row(
  //           children: List.generate(
  //               450 ~/ 9,
  //               (index) => Expanded(
  //                     child: Container(
  //                       color:
  //                           index % 2 == 0 ? Colors.transparent : Colors.grey,
  //                       height: 2,
  //                     ),
  //                   )),
  //         ),
  //         SizedBox(
  //           height: 15,
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: [
  //             Text(
  //               "Total",
  //               style: headingItemMenuStyle,
  //             ),
  //             Text(
  //               (VAT + totalNoVAT).toString(),
  //               style: headingItemMenuStyle,
  //             )
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget payment(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Tổng tiền',
                style: Style().bodyStyle1,
              ),
              Text(
                AppUtil.formatMoney(controller.getTotal()),
                style: Style()
                    .bodyStyle1
                    .copyWith(color: context.theme.primaryColorDark),
                textAlign: TextAlign.end,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'VAT',
                style: Style().bodyStyle1,
              ),
              Text(
                controller.vat.value != null
                    ? controller.vat.value.toString()
                    : '0' + ' %',
                style: Style().bodyStyle1,
                textAlign: TextAlign.end,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Tổng cộng',
                style: Style().bodyStyle1,
              ),
              Text(
                AppUtil.formatMoney(controller.getTotal() -
                    controller.getTotal() *
                        ParseUtil.parseInt(controller.vat.value) /
                        100),
                style: Style().primaryStyle,
                textAlign: TextAlign.end,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: primaryColor, // background
                    onPrimary: primaryLightColor, // foreground
                    shadowColor: Colors.transparent,
                    padding:
                        EdgeInsets.symmetric(horizontal: 30, vertical: 15)),
                onPressed: () => controller.printCarrt(),
                child: Text('In Hoá đơn'),
              ))
        ],
      ),
    );
  }
}
