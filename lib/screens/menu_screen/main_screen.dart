import 'package:animations/animations.dart';
import 'package:get/get.dart';
import 'package:merchant_app/constants.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/screens/menu_screen/menu_screen.dart';
import 'package:merchant_app/screens/menu_screen/payment_details_screen.dart';
import 'package:merchant_app/screens/menu_screen/widget/category_item.dart';
import 'package:merchant_app/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/custom/animatedBottomNavigationBar/animated_bottom_navigation_bar.dart';
import 'package:merchant_app/widgets/custom/animation/indexed_transition_switcher.dart';

class ScreenOne extends GetView<MenuController> {
  ScreenOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: () async {
            return true;
          },
          child: Scaffold(
            body: Row(
              children: [
                Container(
                  color: context.theme.backgroundColor,
                  padding: EdgeInsets.only(left: 16),
                  child: Obx(
                    () => Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(0);
                              },
                              title: 'Trang chủ',
                              icon: Icons.home,
                              index: 0,
                              current: controller.currentIndex.value,
                            ),
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(1);
                              },
                              title: 'Giỏ hàng',
                              icon: Icons.shopping_bag,
                              index: 1,
                              current: controller.currentIndex.value,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(2);
                              },
                              title: 'Menu',
                              icon: Icons.restaurant_menu_outlined,
                              index: 2,
                              current: controller.currentIndex.value,
                            ),
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(3);
                              },
                              title: 'Ví ',
                              icon: Icons.account_balance_wallet_rounded,
                              index: 3,
                              current: controller.currentIndex.value,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(4);
                              },
                              title: 'Lịch sử',
                              icon: Icons.history_sharp,
                              index: 4,
                              current: controller.currentIndex.value,
                            ),
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(5);
                              },
                              title: 'Quà tặng',
                              icon: Icons.card_giftcard,
                              index: 5,
                              current: controller.currentIndex.value,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(6);
                              },
                              title: 'Hoá đơn',
                              icon: Icons.print,
                              index: 6,
                              current: controller.currentIndex.value,
                            ),
                            CategoryItem(
                              onTap: () {
                                controller.setCurrentIndex(7);
                              },
                              title: 'Cài đặt',
                              icon: Icons.settings,
                              index: 7,
                              current: controller.currentIndex.value,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Obx(() {
                  return Expanded(
                    flex: 2,
                    child: IndexedTransitionSwitcher(
                      transitionBuilder: (
                        Widget child,
                        Animation<double> animation,
                        Animation<double> secondaryAnimation,
                      ) {
                        return FadeThroughTransition(
                          fillColor: Colors.transparent,
                          animation: animation,
                          secondaryAnimation: secondaryAnimation,
                          child: child,
                        );
                      },
                      index: controller.currentIndex.value,
                      children: controller.children,
                    ),
                  );
                }),
                Container(
                  height: Get.height,
                  color: context.theme.hintColor.withOpacity(0.5),
                  width: 1,
                ),
                Expanded(flex: 1, child: PaymentDetailsScreen())
              ],
            ),

          ),
        ),
      ),

    );
  }
}





class ImageCard extends StatelessWidget {
  final uri;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(0, 0, 24, 24),
      child: Image.network(uri),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 0.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
    );
  }

  const ImageCard(this.uri);
}
