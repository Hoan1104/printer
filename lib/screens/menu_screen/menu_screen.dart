import 'package:flutter/gestures.dart';
import 'package:get/get.dart';
import 'package:merchant_app/components/menu_item.dart';
import 'package:merchant_app/constants.dart';
import 'package:merchant_app/models/category_model.dart';
import 'package:merchant_app/models/item_menu.dart';
import 'package:merchant_app/models/line_model.dart';
import 'package:merchant_app/models/product_model.dart';
import 'package:merchant_app/provider/current_item_selected.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/screens/menu_screen/payment_details_screen.dart';
import 'package:merchant_app/size_config.dart';
import 'package:flutter/material.dart';
import 'package:merchant_app/utils/app_util.dart';
import 'package:merchant_app/utils/dialog_util.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/_cache.dart';
import 'package:provider/provider.dart';

class MenuScreen extends GetView<MenuController> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Column(
          children: [
            Padding(
                padding: EdgeInsets.only(top: 40, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("Chọn loại món", style: headingStyle),
                    Container(
                      color: context.theme.cardColor,
                      width: Get.width / 3,
                      child: TextField(
                        cursorColor: context.theme.cardColor,
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 8),
                          filled: true,
                          fillColor: Color(0xFFFFFFFF),
                          suffixIcon: Icon(Icons.search,
                              color: Colors.grey.withOpacity(0.7)),
                          //border: InputBorder.none,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          hintText: ' Tìm kiếm',
                          hintStyle: descriptionStyle,
                        ),
                      ),
                    )
                  ],
                )),
            Obx(
              () => Container(
                height: 140,
                margin: const EdgeInsets.all(0.0),
                alignment: Alignment.centerLeft,
                child: TabBar(
                    indicatorWeight: 0.1,
                    // then you can control height of indicator
                    indicatorColor: Colors.transparent,
                    //
                    controller: controller.tabController,
                    isScrollable: true,
                    tabs: controller.listCategory
                        .map<Widget>(
                          (nftModel) => buildItem(context, nftModel),
                        )
                        .toList()),
              ),
            ),
            Expanded(
              child: Obx(
                () => TabBarView(
                  controller: controller.tabController,
                  children: controller.listCategory
                      .map<Widget>(
                        (nftModel) => buildMenu(context),
                      )
                      .toList(),
                ),
              ),
            ),
          ],
        )),
        // PaymentDetailsScreen()

        //for (var i in images) ImageCard(i),
      ],
    );
  }

  Widget buildItem(BuildContext context, CategoryModel categoryModel) {
    return Tab(
      child: Container(
        width: 90,
        child: Column(
          children: [
            CachedImage(
              categoryModel.image?.path ?? '',
              defaultUrl: AppSetting.imgDefault,
              height: 60,
              width: 60,
              radius: 16,
            ),
            SizedBox(height: 8),
            Text(
              categoryModel.name,
              textAlign: TextAlign.start,
              style: Style().normalStyle1,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  Widget buildMenu(BuildContext context) {
    return Obx(() {
      return Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            color: context.theme.cardColor,
            borderRadius: BorderRadius.circular(16)),
        child: GridView.count(
            crossAxisCount: 2,
            // mainAxisSpacing: 8,
            crossAxisSpacing: 16,
            childAspectRatio: 0.99,
            controller: ScrollController(keepScrollOffset: false),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            children: controller.listProduct
                .map<Widget>((i) =>
                    buildItemProduct(context, i,
                        index: controller.listCart
                            .indexWhere((item) => item.product?.id == i.id)))
                .toList()),
      );
    });
  }

  Widget buildItemProduct(BuildContext context, ProductModel model,
      {int? index}) {
    return Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
          ),
          padding: const EdgeInsets.all(20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CachedImage(
                  model.image?.path ?? '',
                  defaultUrl: AppSetting.imgDefault,
                  height: 150,
                  width: 150,
                  fit: BoxFit.cover,
                  radius: 128,
                  boxDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),

                Text(
                  model.name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Style().titleStyle1,
                ),

                Text(
                  AppUtil.formatMoney(model.price),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: Style().primaryStyle,
                ),
                Row(
                  children: [
                    index! <= -1
                        ? Expanded(
                            child: Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: context.theme.primaryColorDark
                                        .withOpacity(0.5), // background
                                    onPrimary: primaryLightColor, // foreground
                                    shadowColor: Colors.transparent,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15)),
                                onPressed: () {
                                  LineModel tempBill = new LineModel(
                                      quantity: 1, product: model);
                                  controller.addItemToBill(tempBill);
                                },
                                child: Text('Thêm vào giỏ hàng'),
                              ),
                            ),
                          )
                        : Expanded(
                            child: Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: context.theme.hintColor
                                        .withOpacity(0.5), // background
                                    onPrimary: primaryLightColor, // foreground
                                    shadowColor: Colors.transparent,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15)),
                                onPressed: () {
                                  LineModel tempBill = new LineModel(
                                      quantity: 1, product: model);
                                  controller.addItemToBill(tempBill);
                                },
                                child: Text('Đã thêm vào giỏ hàng'),
                              ),
                            ),
                          ),
                  ],
                )
              ]),
        ));
  }
}
