import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant_app/components/bill_Item.dart';
import 'package:merchant_app/constants.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/cart_db.dart';
import 'package:merchant_app/databases/config_db.dart';
import 'package:merchant_app/databases/restaurant_db.dart';
import 'package:merchant_app/models/cart_model.dart';
import 'package:merchant_app/models/category_model.dart';
import 'package:merchant_app/models/item_menu.dart';
import 'package:merchant_app/models/line_model.dart';
import 'package:merchant_app/models/product_model.dart';
import 'package:merchant_app/models/restaurant_model.dart';
import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/screens/menu_screen/menu_screen.dart';
import 'package:merchant_app/screens/setting/view/setting.dart';
import 'package:merchant_app/utils/dialog_util.dart';
import 'package:merchant_app/utils/error_util.dart';
import 'package:merchant_app/utils/object_util.dart';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:merchant_app/values/style.dart';

class MenuController extends GetxController with SingleGetTickerProviderMixin {
  final AuthenticateClient client;

  MenuController({required this.client});

  int currentTabsSelected = 1;
  int currentItemSelected = 0;
  late TabController? tabController;
  final listProduct = <ProductModel>[].obs;
  final listCategory = <CategoryModel>[].obs;

  // int currentIndex = 1;
  final currentIndex = 0.obs;
  final listCartAdded = <LineModel>[].obs;
  ScrollController scrollController = new ScrollController();
  List<Widget> children = [];
  List keys = [];
  int selectedIndex = 0;
  final padding = 8.0;
  final tabIndex = 0.obs;
  final listBill = <BillItem>[];
  final total = 0.obs;
  int valueMood = MoodCold;
  int valueSize = SizeS;
  int valueSugar = Sugar30;
  int valueIce = Ice30;
  final restaurant = RestaurantModel().obs;
  final listCart = <LineModel>[].obs;
  final lineModel = LineModel().obs;
  var vat = ConfigDB().getConfigByName(CONFIG_VAT).obs;

  @override
  void onInit() {
    listCart.value = CartDB().getConfigByName(CONFIG_LIST_CART) ?? [];
    restaurant.value = RestaurantDB().currentUser()!;
    tabController = TabController(vsync: this, length: 2);
    fetchPopularProduct();
    fetcxhCategory();
    children = [
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      MenuScreen(),
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      Center(
          child: Text(
        'Chức năng đang được phát triển ',
        style: Style().titleStyle1,
      )),
      SettingPage(),
    ];
    super.onInit();
  }

  changeTabIndex(int index) {
    tabIndex.value = index;
  }

  setCurrentIndex(int index) {
    if (index != currentIndex) {
      currentIndex.value = index;
    }
  }

  Widget get currentPage => children[currentIndex.value];

  onSelectedItem(int index) {
    currentIndex.value = index;
    print(currentIndex.value);
  }

  calculateTotal(listItems) {
    for (var i = 0; i < listItems.length; i++) {
      total.value == listItems[i].priceItem * listItems[i].countItems;
    }
  }

  addItemToBill(LineModel bill) async {
    int indexItem =
        listCart.indexWhere((item) => item.product?.id == bill.product?.id);
    if (indexItem > -1) {
      listCart[indexItem].quantity += 1;
      await CartDB().save(CartModel(CONFIG_LIST_CART, listCart));
      listCart.refresh();
    } else {
      listCart.add(bill);
      await CartDB().save(CartModel(CONFIG_LIST_CART, listCart));
      listCart.refresh();
    }
    print(listCart.first.quantity);
  }

  removeItemOnBill(LineModel bill) async {
    int indexItem =
        listCart.indexWhere((item) => item.product?.id == bill.product?.id);
    ;
    if (indexItem > -1) {
      if (bill.quantity > 1) {
        listCart[indexItem].quantity--;
        await CartDB().save(CartModel(CONFIG_LIST_CART, listCart));
        listCart.refresh();
      } else {
        listCart.removeAt(indexItem);
        await CartDB().save(CartModel(CONFIG_LIST_CART, listCart));

        listCart.refresh();
      }
    }

    calculateTotal(listBill);
  }

  fetchPopularProduct() async {
    await client.getProduct(restaurant.value.id).then((response) async {
      print(response.data);
      if (response.data != null) {
        listProduct.assignAll((response.data['data'] as List)
            .map((e) => ProductModel.fromJson(e as Map<String, dynamic>))
            .toList());
      }
    }).catchError((error, trace) async {
      String? msg = ErrorUtil.getError(error, trace);
      if (ObjectUtil.isNotEmpty(msg)) {
        DialogUtil.showErrorMessage(msg!.tr);
      } else {
        var showErrorMessage = DialogUtil.showErrorMessage('SYSTEM_ERROR'.tr);
      }
    });
  }

  fetcxhCategory() async {
    await client.getCategory(restaurant.value.id).then((response) async {
      print(response.data);
      if (response.data != null) {
        listCategory.assignAll((response.data['data'] as List)
            .map((e) => CategoryModel.fromJson(e as Map<String, dynamic>))
            .toList());
        print(listProduct.value);
      }
    }).catchError((error, trace) async {
      String? msg = ErrorUtil.getError(error, trace);
      if (ObjectUtil.isNotEmpty(msg)) {
        DialogUtil.showErrorMessage(msg!.tr);
      } else {
        var showErrorMessage = DialogUtil.showErrorMessage('SYSTEM_ERROR'.tr);
      }
    });
  }

  double getTotal() {
    return listCart.fold(
        0, (sum, e) => sum + e.quantity * (e.product?.price ?? 0));
  }

  Future<void> printCarrt() async {
    final profile = await CapabilityProfile.load();
    print(profile);
    final generator = Generator(PaperSize.mm80, profile);
    List<int> bytes = [];

    bytes += generator.text(
        'Regular: aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ');
    bytes += generator.text('Special 1: àÀ èÈ éÉ ûÛ üÜ çÇ ôÔ',
        styles: PosStyles(codeTable: 'CP1252'));
    bytes += generator.text('Special 2: blåbærgrød',
        styles: PosStyles(codeTable: 'CP1252'));

    bytes += generator.text('Bold text', styles: PosStyles(bold: true));
    bytes += generator.text('Reverse text', styles: PosStyles(reverse: true));
    bytes += generator.text('Underlined text',
        styles: PosStyles(underline: true), linesAfter: 1);
    bytes +=
        generator.text('Align left', styles: PosStyles(align: PosAlign.left));
    bytes += generator.text('Align center',
        styles: PosStyles(align: PosAlign.center));
    bytes += generator.text('Align right',
        styles: PosStyles(align: PosAlign.right), linesAfter: 1);

    bytes += generator.row([
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col6',
        width: 6,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
    ]);

    bytes += generator.text('Text size 200%',
        styles: PosStyles(
          height: PosTextSize.size2,
          width: PosTextSize.size2,
        ));

    // Print image:
    // final ByteData data = await rootBundle.load('assets/logo.png');
    // final Uint8List imgBytes = data.buffer.asUint8List();

    // Print image using an alternative (obsolette) command
    // bytes += generator.imageRaster(image);

    // Print barcode
    final List<int> barData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 4];
    bytes += generator.barcode(Barcode.upcA(barData));

    // Print mixed (chinese + latin) text. Only for printers supporting Kanji mode
    // ticket.text(
    //   'hello ! 中文字 # world @ éphémère &',
    //   styles: PosStyles(codeTable: PosCodeTable.westEur),
    //   containsChinese: true,
    // );

    bytes += generator.feed(2);
    bytes += generator.cut();
  }
}
