import 'package:get/get.dart';
import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/screens/menu_screen/controller/menu_controller.dart';
import 'package:merchant_app/screens/setting/controller/setting_controller.dart';

class MenuBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MenuController(client: AuthenticateClient()));
    Get.lazyPut(() => SettingController(client: AuthenticateClient()));
  }

}