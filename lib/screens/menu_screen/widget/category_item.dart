import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant_app/values/style.dart';

class CategoryItem extends StatelessWidget {
  final GestureTapCallback onTap;
  final IconData icon;
  final String title;
  final int index;
  final int current;

  CategoryItem(
      {required this.onTap,
      required this.icon,
      required this.title,
      required this.index,
      required this.current});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        width: 100,
        margin: EdgeInsets.only(bottom: 16, right: 16),
        decoration: BoxDecoration(
            color:
                index == current ? Color(0xffFF6465) : context.theme.cardColor,
            borderRadius: BorderRadius.circular(16)),
        padding: EdgeInsets.symmetric(horizontal: 3.0, vertical: 3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              constraints: BoxConstraints(
                maxHeight: 64,
                maxWidth: 64,
                minHeight: 64,
                minWidth: 64,
              ),
              alignment: Alignment.center,
              child: Icon(
                icon,
                size: 36,
                color: index == current
                    ? context.theme.backgroundColor
                    : context.theme.hintColor,
              ),
            ),
            Text(
              title.tr,
              textAlign: TextAlign.center,
              style: Style().subtitleStyle1.copyWith(
                  fontSize: 13,
                  letterSpacing: 0.07,
                  fontFamily: Style.fontDisplayBold,
                  color: index == current
                      ? context.theme.backgroundColor
                      : context.theme.hintColor),
            ),
            SizedBox(
              height: 16,
            )
          ],
        ),
      ),
    );
  }
}
