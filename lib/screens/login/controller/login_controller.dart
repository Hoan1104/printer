import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant_app/constants/config_constant.dart';
import 'package:merchant_app/databases/config_db.dart';
import 'package:merchant_app/databases/restaurant_db.dart';
import 'package:merchant_app/models/config_model.dart';
import 'package:merchant_app/models/restaurant_model.dart';
import 'dart:core';

import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/routes/app_pages.dart';
import 'package:merchant_app/utils/dialog_util.dart';
import 'package:merchant_app/utils/error_util.dart';
import 'package:merchant_app/utils/object_util.dart';

class LoginController extends GetxController with SingleGetTickerProviderMixin {
  final AuthenticateClient client;

  LoginController({
    required this.client,
  });

  final isShowAction = false.obs;
  late TabController tabController;
  final tabSelected = 0.obs;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController keywordController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController userName = TextEditingController();
  final TextEditingController passRegister = TextEditingController();
  final TextEditingController otpController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final FocusNode emailFocus = FocusNode();
  final FocusNode keywordFocus = FocusNode();
  final FocusNode passRegisterFocus = FocusNode();
  final FocusNode userNameFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode phoneFocus = FocusNode();
  final FocusNode otpFocus = FocusNode();
  var loading = false.obs;
  var isShowPassword = false.obs;
  var keywordValid = true.obs;
  var passwordValid = true.obs;
  var otpValid = true.obs;
  var codeValue = '84'.obs;
  var emailErrorMsg = 'invalid_email'.tr.obs;
  var passErrorMsg = 'invalid_password'.tr.obs;
  var otpErrorMsg = 'invalid_otp'.tr.obs;
  var verifyTime = DateTime.now().toLocal().millisecondsSinceEpoch.obs;
  final isShowPasswordRegister = false.obs;
  final emailValid = true.obs;
  final referralValid = true.obs;
  final passValid = true.obs;
  final usernameValid = true.obs;
  final hasUppercase = false.obs;
  final hasLowercase = false.obs;
  final hasDigit = false.obs;
  final hasMinLength = false.obs;
  final hasMinLengthUserName = false.obs;
  final policyValid = true.obs;
  final isChecked = false.obs;
  TapGestureRecognizer? termsRecognizer;

  @override
  void onInit() {
    termsRecognizer = TapGestureRecognizer()..onTap = openTerms;
    tabController = TabController(vsync: this, length: 2);
    super.onInit();
    keywordController.text =
        ConfigDB().getConfigByName(CONFIG_LAST_USERNAME) ?? '';
    String? username = ConfigDB().getConfigByName(CONFIG_LAST_USERNAME);
    if (ObjectUtil.isNotEmpty(username)) {
      keywordController.text = username!;
    } else {
      keywordController.clear();
    }
  }

  openTerms() {
    // launch(LINK_TERM_OF_USE);
  }

  @override
  void onClose() {
    keywordController.dispose();
    passwordController.dispose();
    otpController.dispose();
    keywordFocus.dispose();
    passwordFocus.dispose();
    otpFocus.dispose();
    super.onClose();
  }

  onChecked(bool value) {
    isChecked.value = value;
    policyValid.value = value;
  }

  showAction() {
    isShowAction.value = !isShowAction.value;
    print(isShowAction.value);
  }

  onChangeText(String text) {
    keywordValid.value = keywordController.text.isNotEmpty;

    // if (passwordController.text.isEmpty) {
    //   passErrorMsg.value = 'invalid_password'.tr;
    // }
    // passwordValid.value = passwordController.text.isNotEmpty;
  }

  onChangeEmai(String text) {
    emailController.text.isNotEmpty;

    // if (passwordController.text.isEmpty) {
    //   passErrorMsg.value = 'invalid_password'.tr;
    // }
    // passwordValid.value = passwordController.text.isNotEmpty;
  }

  onChangName(String text) {
    userName.text.isNotEmpty;

    // if (passwordController.text.isEmpty) {
    //   passErrorMsg.value = 'invalid_password'.tr;
    // }
    // passwordValid.value = passwordController.text.isNotEmpty;
  }

  onChangePhone(String text) {
    phoneController.text.isNotEmpty;
  }

  onChangePass(String pass) {
    passwordValid.value = passwordController.text.isNotEmpty;
  }

  onSelectedTabPage(int tab) async {
    tabSelected.value = tab;
  }

  onSubmitLogin() async {
    if (keywordController.text.isEmpty) {
      keywordValid.value = false;
      emailErrorMsg.value = 'Email không hợp lệ'.tr;
      DialogUtil.showErrorMessage(emailErrorMsg.value);
      return;
    }

    if (passwordController.text.isEmpty) {
      passwordValid.value = false;
      passErrorMsg.value =
          'Mật khẩu phải chứa ít nhất 8 kí tự và bao gồm ít nhất 1 kí tự in hoa, 1 kí tự in thường , 1 kí tự số.'
              .tr;
      DialogUtil.showErrorMessage(passErrorMsg.value);
      return;
    }
    if (isValid()) {
      loading.value = true;
      await client
          .login((keywordController.text), passwordController.text)
          .then((response) async {
        print(response);
        print(response.data['message']);
        print(response.statusCode);
        loading.value = false;
        if (response.statusCode == 200 && response.data['message'] == null) {
          ConfigDB().deleteConfigByName(CONFIG_REGISTER_USERNAME);
          ConfigDB().deleteConfigByName(CONFIG_YOUR_ADRESS);
          await ConfigDB().save(
              ConfigModel(CONFIG_ACCESS_TOKEN, response.data['data']['token']));
          print(response.data['data']['token']);
          await ConfigDB()
              .save(ConfigModel(CONFIG_USERNAME, keywordController.text));

          await client.getMe().then((response) async {
            print(response);
            await RestaurantDB()
                .save(RestaurantModel.fromJson(response.data['data']));
            loading.value = false;
            DialogUtil.showSuccessToast('login_success'.tr);
            Get.offAllNamed(Routes.HOME);
          }).catchError((error, trace) {
            loading.value = false;
            String? msg = ErrorUtil.getError(error, trace);
          });
        } else {
          DialogUtil.showErrorMessage(response.data['message']);
        }
      }).catchError((error, trace) {
        loading.value = false;
        ErrorUtil.catchError(error, trace);
      });

      loading.value = false;
    }
  }

  bool isValid() {
    return keywordValid.value &&
        passwordValid.value &&
        keywordController.text.isNotEmpty &&
        passwordController.text.isNotEmpty;
  }

  onResendOTP(bool isCountdown) async {}

  onVerifyOTP() async {
    if (loading.isFalse) {
      loading.value = true;
    }
  }

  onBack() {
    Get.back();
  }

  onForgotPass() {
    Get.toNamed(Routes.FORGOT_PASS);
  }

  onGoToRegister() {
    Get.toNamed(Routes.REGISTER);
  }

  resetFocus() {
    FocusScopeNode currentFocus = FocusScope.of(Get.context!);

    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  toggleShowPassword() {
    isShowPassword.value = !isShowPassword.value;
  }
}
