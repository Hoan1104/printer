import 'package:get/get.dart';
import 'package:merchant_app/repositories/authenticate_client.dart';
import 'package:merchant_app/screens/login/controller/login_controller.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController(
          client: AuthenticateClient(),
        ));
  }
}
