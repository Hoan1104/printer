import 'package:merchant_app/routes/app_pages.dart';
import 'package:merchant_app/screens/login/controller/login_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/_default_header.dart';
import 'package:merchant_app/widgets/_radius_button.dart';
import 'package:merchant_app/widgets/_suggestion_text_field.dart';
import 'package:merchant_app/widgets/_title_default_text_field.dart';

class LoginFormWidget extends GetView<LoginController> {
  const LoginFormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 25),
      decoration: BoxDecoration(
        color: context.theme.backgroundColor,
        borderRadius: BorderRadius.circular(16),
      ),
      width: 400,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DefaultHeader(
            padding: EdgeInsets.only(top: 24, bottom: 32),
            title: 'Đăng nhập'.tr,
            description: 'Đăng nhập bằng email của bạn'.tr,
          ),
          Obx(() => TitleDefaultTextField(
                textInputType: TextInputType.phone,
                width: 350,
                radius: 10,
                controller: controller.keywordController,
                focusNode: controller.keywordFocus,
                // labelText: 'email'.tr,
                hintText: 'Nhập Email của bạn'.tr,
                errorMsg: controller.keywordValid.value
                    ? null
                    : controller.emailErrorMsg.value,
                innerPadding:
                    EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 20),
                outsidePadding: EdgeInsets.only(bottom: 16),
                onChanged: (text) {
                  controller.onChangeText(text);
                },
              )),
          Obx(() => TitleDefaultTextField(
                width: 350,
                radius: 10,
                controller: controller.passwordController,
                focusNode: controller.passwordFocus,
                hintText: 'Nhập mật khẩu  '.tr,
                obscureText: !controller.isShowPassword.value,
                autoFillHints: const [AutofillHints.password],
                errorMsg: controller.passwordValid.value
                    ? null
                    : controller.passErrorMsg.value,
                innerPadding:
                    EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 20),
                outsidePadding: EdgeInsets.only(bottom: 16),
                suffix: Icon(
                  controller.isShowPassword.value
                      ? Icons.remove_red_eye_outlined
                      : Icons.remove_red_eye,
                  size: 16,
                  color: context.theme.iconTheme.color!.withOpacity(0.5),
                ),
                onTabSuffix: controller.toggleShowPassword,
                onChanged: (text) {
                  controller.onChangePass(text);
                },
              )),
          buildFooter(context)
        ],
      ),
    );
  }

  Widget buildHeaderPage(BuildContext context) {
    return Container(
      height: 44,
      margin: const EdgeInsets.all(0.0),
      alignment: Alignment.centerLeft,
      color: context.textTheme.overline!.color,
      child: TabBar(
        controller: controller.tabController,
        isScrollable: false,
        dragStartBehavior: DragStartBehavior.down,
        labelColor: context.textTheme.headline1!.color,
        indicatorColor: context.theme.primaryColor,
        indicatorWeight: 3.0,
        unselectedLabelColor: context.theme.hintColor,
        labelStyle: Style().noteStyle1,
        labelPadding: EdgeInsets.all(9.0),
        tabs: [
          Tab(text: 'Pickup'.tr),
          Tab(text: 'dilivery'.tr),

          // Tab(text: 'Dinein'.tr),
        ],
        onTap: controller.onSelectedTabPage,
      ),
    );
  }

  Widget buildFooter(BuildContext context) {
    return Obx(
      () => RadiusButton(
          isFullWidth: true,
          isDisable: !controller.keywordValid.value ||
              !controller.passwordValid.value ||
              controller.keywordController.text.isEmpty ||
              controller.passwordController.text.isEmpty,
          maxWidth: 350,
          radius: 10,
          outsidePadding: EdgeInsets.only(
            top: 23,
            bottom: 20,
          ),
          innerPadding: controller.loading.value
              ? EdgeInsets.only(top: 14, bottom: 14, left: 20, right: 20)
              : EdgeInsets.only(
                  top: 16,
                  bottom: 16,
                  left: 5,
                  right: 5,
                ),
          isLoading: controller.loading.value,
          indicatorSize: 24,
          text: 'Đăng nhập'.tr,
          fontFamily: Style.fontDemiBold,
          fontSize: 17,
          backgroundColor: context.theme.primaryColor,
          textColor: context.textTheme.headline5!.color,
          onTap: controller.onSubmitLogin),
    );
  }
}
