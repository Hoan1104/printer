import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:merchant_app/helpers/extension/reposive_helper.dart';
import 'package:merchant_app/screens/login/controller/login_controller.dart';
import 'package:merchant_app/screens/login/widget/login_form.dart';
import 'package:merchant_app/screens/login/widget/register_form.dart';
import 'package:merchant_app/values/setting.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: context.theme.backgroundColor,
        child: Scaffold(
          backgroundColor: const Color(0xffF0F2F5),
          body: ResponsiveWidget(
            desktop: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Lottie.asset(
                  AppSetting.login,
                  width: 300,
                  height: 300,
                  fit: BoxFit.scaleDown,
                  repeat: true,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    LoginFormWidget(),
                  ],
                ),

                // Center(
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: [LoginFormWidget()],
                //   ),
                // ),
              ],
            ),
            mobile: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  LoginFormWidget(),
                ],
              ),
            ),
          ),
        ));
  }
}
