import 'package:merchant_app/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PaymentMethodButton extends StatefulWidget {
  final String paymentMethod;
  final String paymentMethodIcon;

  const PaymentMethodButton(
      {Key? key, required this.paymentMethod, required this.paymentMethodIcon})
      : super(key: key);

  @override
  _PaymentMethodButtonState createState() =>
      _PaymentMethodButtonState(paymentMethod, paymentMethodIcon);
}

class _PaymentMethodButtonState extends State<PaymentMethodButton> {
  final String paymentMethod;
  final String paymentMethodIcon;

  _PaymentMethodButtonState(this.paymentMethod, this.paymentMethodIcon);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(right: 10),
            child: Container(
              width: 100,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.transparent, width: 1.5),
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: [
                  SvgPicture.asset(paymentMethodIcon, height: 35),
                  Text(
                    paymentMethod,
                    style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 12),
                  ),
                ],
              ),
            )),
      ],
    );
  }
}
