import 'package:get/get.dart';
import 'package:merchant_app/constants.dart';
import 'package:merchant_app/models/product_model.dart';
import 'package:merchant_app/size_config.dart';
import 'package:flutter/material.dart';
import 'package:merchant_app/utils/app_util.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';
import 'package:merchant_app/widgets/_cache.dart';

class MenuItem extends GetView {
  const MenuItem(
      {Key? key,
      required this.id,
      required this.imgUrl,
      required this.billTitle,
      required this.description,
      required this.priceItem,
      required this.productModel,
      this.index,
      this.onPress})
      : super(key: key);

  final ProductModel productModel;
  final String id;
  final String imgUrl;
  final String billTitle;
  final String description;
  final double priceItem;
  final int? index;
  final VoidCallback? onPress;

//----------------------------------------
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: IntrinsicHeight(
            child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
          ),
          width: sizeWidthMenuItem(),
          padding: const EdgeInsets.all(20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _ItemDetails(
                  imgUrl: imgUrl,
                  billTitle: billTitle,
                  desc: description,
                  priceItem: priceItem,
                ),
                Column(
                  children: productModel.attributes
                      .map<Widget>(
                        (attributes) => Text(
                          productModel.attributes.length.toString(),
                          style: Style().primaryStyle,
                        ),
                      ).toList(),
                ),
                Row(
                  children: [
                    index! <= -1
                        ? Expanded(
                            child: Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: context.theme.primaryColorDark
                                        .withOpacity(0.5), // background
                                    onPrimary: primaryLightColor, // foreground
                                    shadowColor: Colors.transparent,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15)),
                                onPressed: onPress,
                                child: Text('Thêm vào giỏ hàng'),
                              ),
                            ),
                          )
                        : Expanded(
                            child: Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: context.theme.hintColor
                                        .withOpacity(0.5), // background
                                    onPrimary: primaryLightColor, // foreground
                                    shadowColor: Colors.transparent,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 15)),
                                onPressed: onPress,
                                child: Text('Đã thêm vào giỏ hàng'),
                              ),
                            ),
                          ),
                  ],
                )
              ]),
        )));
  }
}

class _ItemDetails extends StatelessWidget {
  final String imgUrl;
  final String billTitle;
  final String desc;
  final double priceItem;

  const _ItemDetails(
      {Key? key,
      required this.imgUrl,
      required this.billTitle,
      required this.desc,
      required this.priceItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Container(
        width: (SizeConfig.screenWidth -
                payment_Details_Screen -
                navigation_Rail) /
            2,
        child: Column(
          children: [
            CachedImage(
              imgUrl,
              defaultUrl: AppSetting.imgDefault,
              height: 150,
              width: 150,
              fit: BoxFit.cover,
              radius: 128,
              boxDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              billTitle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Style().titleStyle1,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              AppUtil.formatMoney(priceItem),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: Style().primaryStyle,
            ),
          ],
        ));
  }
}

class MyRadioListTile<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final Widget leading;
  final ValueChanged<T?> onChanged;

  const MyRadioListTile({
    required this.value,
    required this.groupValue,
    required this.onChanged,
    required this.leading,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChanged(value),
      child: Container(
        height: 56,
        //padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: [
            _customRadioButton,
            SizedBox(width: 6),
          ],
        ),
      ),
    );
  }

  Widget get _customRadioButton {
    final isSelected = value == groupValue;
    return Container(
      width: 32,
      height: 32,
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
        color: backgroundOptionButtonColor,
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: isSelected
              ? primaryColor.withOpacity(0.5)
              : backgroundOptionButtonColor,
          width: 2,
        ),
      ),
      child: leading,
    );
  }
}
