import 'package:merchant_app/size_config.dart';
import 'package:flutter/material.dart';

const double payment_Details_Screen = 300;
const double navigation_Rail = 80;

const MoodCold = 1;
const MoodHot = 2;
const SizeS = 1;
const SizeM = 2;
const SizeL = 3;
const Sugar30 = 1;
const Sugar50 = 2;
const Sugar70 = 3;
const Ice30 = 1;
const Ice50 = 2;
const Ice70 = 3;

const primaryColor = Color(0xff704232);
const primaryLightColor = Color(0xfff5efef);
const backgroundColor = Color(0xffececf1);
const backgroundOptionButtonColor = Color(0xfff6f0f2);
const primaryTextColor = Color(0xff772f1a);
const waringTextColor = Color(0xffd8572a);
const dangerTextColor = Color(0xffc32f27);

final headingStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.black.withOpacity(0.8),
  height: 1.8,
);

final headingItemMenuStyle = TextStyle(
  fontWeight: FontWeight.w800,
  color: Colors.black.withOpacity(0.8),
  fontSize: 20,
  height: 1.8,
);

final titleStyle = TextStyle(
  color: Colors.black,
  height: 1.5,
);

final descriptionStyle = TextStyle(
  fontWeight: FontWeight.w500,
  color: Colors.grey,
  height: 1.3,
);

TextStyle textLeadingMenuItem(double fontSize, Color color) {
  return TextStyle(
      fontSize: fontSize, fontWeight: FontWeight.w600, color: color);
}
