import 'package:flutter/material.dart';
import 'package:merchant_app/values/style.dart';

class ErrorTextField extends StatelessWidget {
  final bool? isValid;
  final String? errorMsg;
  final String? fontFamily;
  final double? fontSize;
  final Color? color;
  final TextAlign? textAlign;
  final EdgeInsets? padding;

  const ErrorTextField({
    this.isValid = true,
    this.errorMsg,
    this.fontFamily = Style.fontRegular,
    this.fontSize = 14,
    this.color = Colors.red,
    this.textAlign = TextAlign.left,
    this.padding = const EdgeInsets.all(0),
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: isValid! ? 0.0 : 1.0,
      duration: const Duration(milliseconds: 250),
      child: Text(
        errorMsg ?? '',
      ),
    );
  }
}
