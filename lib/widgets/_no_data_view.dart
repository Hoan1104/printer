import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant_app/values/setting.dart';
import 'package:merchant_app/values/style.dart';

class NoDataView extends StatelessWidget {
  final String? message;

  const NoDataView({this.message, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 10),
          Image.asset(
            AppSetting.imgNoData,
            width: 150,
            height: 120,
            color: context.isDarkMode ? context.iconColor : null,
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10, left: 16, right: 16),
            child: Text(
              (message == null) ? 'we_could_not_find_your_data'.tr : message!,
              style: Style().noteStyle1.copyWith(
                    fontFamily: Style.fontRegular,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
