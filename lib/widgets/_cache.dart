
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';



class CachedImage extends StatelessWidget {
  final String url;
  final String? defaultUrl;
  final String? blurHash;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final String? token;

  final Duration shimmerDuration;
  final Color? shimmerBaseColor;
  final Color? shimmerHighlightColor;
  final Color? shimmerBackColor;
  final Widget? errorWidget;
  final BoxDecoration? boxDecoration;
  final double radius;

  const CachedImage(
      this.url, {
        this.defaultUrl,
        this.blurHash,
        this.width = 300,
        this.height = 300,
        this.fit = BoxFit.fill,
        this.token,
        this.shimmerDuration = const Duration(milliseconds: 1500),
        this.shimmerBaseColor,
        this.shimmerHighlightColor,
        this.shimmerBackColor,
        this.errorWidget,
        this.boxDecoration,
        this.radius = 0,
        Key? key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      width: width,
      height: height,
      memCacheWidth: width?.toInt(),
      memCacheHeight: height?.toInt(),
      maxWidthDiskCache: width?.toInt(),
      maxHeightDiskCache: height?.toInt(),
      fit: fit,
      imageUrl: url,
      httpHeaders: {
        'Authorization': token ?? '',
      },
      imageBuilder: (context, imageProvider) => Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          image: DecorationImage(
            image: imageProvider,
            fit: fit,
          ),
        ),
      ),

      errorWidget: (context, url, error) {
        return Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            image: DecorationImage(
              image: ResizeImage(
                AssetImage(defaultUrl!),
                width: width?.toInt(),
                height: height?.toInt(),
              ),
              fit: BoxFit.fill,
            ),
          ),
        );
      },
    );
  }
}
