import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class OpenContainerWrapper extends StatelessWidget {
  const OpenContainerWrapper({
    required this.closedBuilder,
    required this.transitionType,
    this.onClosed,
    this.nextScreen,
    this.openColor,
    this.closedColor,
    this.openShape,
    this.closedShape,
    this.routeSettings,
    this.useRootNavigator = false,
    this.transitionDuration = const Duration(milliseconds: 300),
    Key? key,
  }) : super(key: key);

  final CloseContainerBuilder closedBuilder;
  final ContainerTransitionType transitionType;
  final ClosedCallback<bool?>? onClosed;
  final Widget? nextScreen;
  final Color? openColor;
  final Color? closedColor;
  final ShapeBorder? openShape;
  final ShapeBorder? closedShape;
  final RouteSettings? routeSettings;
  final bool useRootNavigator;
  final Duration transitionDuration;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      useRootNavigator: useRootNavigator,
      routeSettings: routeSettings,
      openColor: openColor!,
      closedColor: closedColor!,
      middleColor: Colors.transparent,
      closedShape: openShape!,
      openShape: closedShape!,
      openElevation: 0.0,
      closedElevation: 0.0,
      transitionType: transitionType,
      openBuilder: (BuildContext context, VoidCallback _) {
        return nextScreen!;
      },
      onClosed: onClosed,
      tappable: false,
      closedBuilder: closedBuilder,
      transitionDuration: transitionDuration,
    );
  }
}
