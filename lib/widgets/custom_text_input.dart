import 'package:flutter/material.dart';
import 'package:merchant_app/values/colors.dart';

class CustomTextInput extends StatelessWidget {
  final String hintText;
  final EdgeInsets? padding;


  CustomTextInput({required this.hintText, this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      decoration: ShapeDecoration(
        color: AppColor.placeholderBg,
        shape: StadiumBorder(),
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText,
          hintStyle: TextStyle(
            color: AppColor.placeholder,
          ),
          contentPadding: padding,
        ),
      ),
    );
  }
}
