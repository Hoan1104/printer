class AppSetting {
  AppSetting._();

  // Url

  //Lotti
  static const lottieSuccess = 'assets/lotties/success.json';
  static const lottieConfirm = 'assets/lotties/confirm.json';
  static const lottieWarning = 'assets/lotties/warning.json';
  static const lottieMaintain = 'assets/lotties/maintain.json';
  static const lottieTradingLoading = 'assets/lotties/trading_loading.json';
  static const lottieNFTLoading = 'assets/lotties/nft_loading.json';
  static const lottieSuccessGreen = 'assets/lotties/success_green.json';
  static const lottieSearching = 'assets/lotties/lotties_searching.json';
  static const lottieSpalsh= 'assets/lotties/splash_loading.json';
  static const location= 'assets/lotties/your_location.json';
  static const login= 'assets/lotties/login.json';

  static const imgLogo = 'assets/img_logo_white.png';
  static const imgLogoText = 'assets/img_logo_text.png';
  static const imgLogoNgang = 'assets/logo_hitrade_ngang.png';
  static const imgLogoHidden = 'assets/img_background_share.png';
  static const imgLogoBlack = 'assets/img_background_black.png';
  static const imgIntro4 = 'assets/img_story_board.png';
  static const imgVerify = 'assets/img_verify.png';
  static const imgTakePhotoBack = 'assets/img_back_cart.png';
  static const imgTakePhotoFront = 'assets/img_front_cart.png';
  static const imgTakePassport = 'assets/img_take_pass_post.png';
  static const imgTakePortrait = 'assets/img_take_persion.png';
  static const imgGuidePhoto = 'assets/img_guide_photo.png';
  static const imgGuidePortrait = 'assets/img_guide_document.png';
  static const imgBannerReferral = 'assets/img_share_app.png';
  static const imgNoData = 'assets/img_no_data.png';
  static const imgBannerWallet = 'assets/imgWalletCommo.png';
  static const imgWarning = 'assets/img_warning.png';
  static const imgLogoWhite = 'assets/logo_hitrade_white.png';
  static const imgSplashDark = 'assets/img_splash_dark.png';
  static const imgSplashLight = 'assets/img_splash_light.png';
  static const imgOnBoarding = 'assets/img_onboarding.png';
  static const imgSuggest = 'assets/img_suggest.png';

  static const imgIntro1 = 'assets/img_new_intro1.png';
  static const imgIntro2 = 'assets/img_new_intro2.png';
  static const imgIntro3 = 'assets/img_new_intro3.png';
  static const imgHiTrade = 'assets/img_hitrade.png';
  static const imgHiEarn = 'assets/img_hiearn.png';
  static const imgHiCommodity = 'assets/img_hicommodity.png';
  static const icAlert2Outlined = 'assets/ic_alert_2_outlined.png';
  static const icArrowDown = 'assets/ic_arrow_down.png';
  static const icArrowLeft = 'assets/ic_arrow_left.png';
  static const icArrowRight = 'assets/ic_arrow_right.png';
  static const icArrowUp = 'assets/ic_arrow_up.png';
  static const icCameraFilled = 'assets/ic_camera_filled.png';
  static const icCapDown = 'assets/ic_cap_down.png';
  static const icCheck = 'assets/ic_check.png';
  static const icChevronDown = 'assets/ic_drop_down.png';
  static const icChevronRight = 'assets/ic_chevron_right.png';
  static const icClose = 'assets/ic_close.png';
  static const icEditFilled = 'assets/ic_edit.png';
  static const icEmbed = 'assets/ic_embed.png';
  static const icEyeFilled = 'assets/ic_eye_filled.png';
  static const icEyeOutlined = 'assets/ic_eye_outlined.png';
  static const icFacebook = 'assets/ic_facebook.png';
  static const icFacebookOutlined = 'assets/ic_facebook_outline.png';
  static const icFileOutlined = 'assets/ic_file_outlined.png';
  static const icHistory = 'assets/ic_history.png';
  static const icHomeOutlined = 'assets/ic_home_outline.png';
  static const icHomeFilled = 'assets/ic_home_filled.png';
  static const icLocationOutlined = 'assets/ic_location_outlined.png';
  static const icLockOutlined = 'assets/ic_lock_outlined.png';
  static const icMailOutlined = 'assets/ic_mail_outlined.png';
  static const icMenu = 'assets/ic_menu.png';
  static const icMainMenu = 'assets/ic_more.png';
  static const icMainMenuDark = 'assets/ic_menu_dark.png';
  static const icMessengerOutlined = 'assets/ic_messenger_outlined.png';
  static const icMinus = 'assets/ic_minus.png';
  static const icPersonOutlined = 'assets/ic_person_outlined.png';
  static const icPhoneOutlined = 'assets/ic_phone_outlined.png';
  static const icPlus = 'assets/ic_plus.png';
  static const icPlusCircle = 'assets/ic_plus_circle.png';
  static const icSearch = 'assets/ic_search.png';
  static const icShareOutlined = 'assets/ic_share_outlined.png';
  static const icStarFilled = 'assets/ic_star_yellow.png';
  static const icStarOutlined = 'assets/ic_star_outlined.png';
  static const icUpload = 'assets/ic_upload.png';
  static const icWalletOutlined = 'assets/ic_wallet_outline.png';
  static const icWorldOutlined = 'assets/ic_world_outlined.png';
  static const icShareOutlined2 = 'assets/ic_share_outlined_2.png';
  static const icArrowRightLeft = 'assets/ic_arrow_right_left.png';
  static const icReferral = 'assets/ic_referral.png';
  static const icQuestion = 'assets/ic_question.png';
  static const icVerifyColor = 'assets/ic_verify.png';
  static const icBank = 'assets/ic_bank.png';
  static const icQuestionWarning = 'assets/icon_Question.png';
  static const icTradeOutlined = 'assets/ic_deal_outline.png';
  static const icTradeFilled = 'assets/ic_deal_filled.png';
  static const icAnalystOutlined = 'assets/ic_identify_outline.png';
  static const icAnalystFilled = 'assets/ic_identify_filled.png';
  static const icAccountOutlined = 'assets/ic_user_outline.png';
  static const icAccountFilled = 'assets/ic_user_filled.png';
  static const icChartOutlined = 'assets/ic_graph.png';
  static const icChartFilled = 'assets/ic_graph_filled.png';
  static const icWalletFilled = 'assets/ic_wallet_filled.png';
  static const icCandle = 'assets/ic_chart.png';
  static const icShare = 'assets/ic_upload.png';
  static const icDarkMode = 'assets/ic_dark_mode.png';
  static const icLightMode = 'assets/ic_light_mode.png';
  static const icWallet = 'assets/ic_wallet.png';
  static const icWalletDark = 'assets/ic_wallet_dark.png';
  static const icMXVWallet = 'assets/ic_mxv_wallet.png';
  static const icMXVWalletDark = 'assets/ic_mxv_wallet_dark.png';
  static const icTransfers = 'assets/ic_transfer.png';
  static const icDeposit = 'assets/ic_recharge.png';
  static const icWithDrawal = 'assets/ic_withdrawal.png';
  static const icUser = 'assets/ic_user.png';
  static const icNdttBuy = 'assets/ic_buy.png';
  static const icNdttSell = 'assets/ic_sell.png';
  static const icNdttStopLoss = 'assets/ic_stop.png';
  static const icRiskLow = 'assets/ic_risk_low.png';
  static const icRiskMid = 'assets/ic_risk_mid.png';
  static const icRiskHigh = 'assets/ic_risk_high.png';
  static const icSaving = 'assets/ic_saving.png';
  static const icMargin = 'assets/ic_margin.png';
  static const icFunding = 'assets/ic_funding.png';
  static const icAccountLight = 'assets/ic_account_light.png';
  static const icAccountDark = 'assets/ic_account_dark.png';
  static const icFlashNews = 'assets/ic_flash_news.png';
  static const icWatermark = 'assets/ic_watermark.png';
  static const icSavingLight = 'assets/icon_saving_light.png';
  static const icSavingDark = 'assets/icon_saving_dark.png';
  static const icSwitchMode = 'assets/ic_switch_mode.png';
  static const icDeleteKeyboard = 'assets/ic_delete_keyboard.png';
  static const icToken = 'assets/ic_token.png';

  //NFT Marketplace
  static const lottieUploading = 'assets/lotties/uploading.json';
  static const imgNFTOnBoarding = 'assets/img_nft_onboarding.png';
  static const imgNFTDefault = 'assets/img_nft_default.png';
  static const imgLogoNFTLight = 'assets/img_nft_logo_light.png';
  static const imgLogoNFTDark = 'assets/img_nft_logo_light.png';
  static const icAuction = 'assets/ic_auction.png';
  static const icHeart = 'assets/ic_heart_outline.png';
  static const icHeartFilled = 'assets/ic_heart_filled.png';
  static const icNFT = 'assets/ic_nft.png';
  static const icTag = 'assets/ic_tag.png';
  static const icNFTAbout = 'assets/ic_nft_about.png';
  static const icSwitch = 'assets/ic_switch.png';
  static const icDots = 'assets/ic_dots.png';
  static const icView = 'assets/ic_view.png';

  static const icVerifyId = 'assets/ic_id_card.png';
  static const icGift = 'assets/ic_gift.png';
  static const icNotification = 'assets/ic_bell.png';
  static const icSecurity = 'assets/ic_security.png';
  static const icHelpAndSupport = 'assets/ic_support.png';
  static const icAbout = 'assets/ic_info.png';
  static const icCamera = 'assets/ic_camera_filled.png';
  static const icVerifyPhone = 'assets/ic_verify_phone.png';
  static const icAddDocument = 'assets/ic_add_document.png';
  static const icPersonalInformation = 'assets/ic_personal_information.png';
  static const icInfoAnimation = 'assets/ic_info.png';
  static const icCopy = 'assets/ic_copy.png';
  static const icStatistical = 'assets/ic_analytics.png';
  static const icListRef = 'assets/ic_chart_network.png';
  static const icMail = 'assets/ic_mail.png';
  static const icFailed = 'assets/ic_info_circle.png';
  static const icChangeStyle = 'assets/icons/ic_moon.';
  static const icImgContact = 'assets/img_contract.png';
  static const icImgHiMargin = 'assets/bannerhimargin.png';
  static const icHelp = 'assets/ic_contact.png';
  static const icCmnd = 'assets/img_cmnd.png';
  static const icPassPost = 'assets/img_pass_port.png';
  static const icSuccess = 'assets/ic_success.png';
  static const icCalendar = 'assets/ic_lich.png';
  static const icBot = 'assets/ic_bot.png';

  static const icEditOutLine = 'assets/ic_edit_out_line.png';
  static const icSuccessFilled = 'assets/ic_success_filled.png';
  static const icNext = 'assets/ic_next.png';
  static const imgGift = 'assets/img_gift.png';
  static const imgCoupon = 'assets/ic_voucvher.png';
  static const icLike = 'assets/ic_like.png';
  static const icAboutUs = 'assets/ic_about_us.png';
  static const icMoney = 'assets/ic_money.png';
  static const icCoupon = 'assets/ic_coupon.png';
  static const icYourCoupon = 'assets/ic_your_coupon.png';
  static const icTicket = "assets/img_ticket.png";

  static const icVerify = 'assets/ic_verified_creator.png';
  static const icCreator = 'assets/ic_creator.png';
  static const icNotVerify = 'assets/ic_tick_yellow.png';
  static const icTopOne = 'assets/ic_gold.png';
  static const icTopTwo = 'assets/ic_sliver.png';
  static const icTopThree = 'assets/ic_top_three.png';
  static const icLongDown = 'assets/ic_long_down.png';
  static const icLongUp = 'assets/ic_long_bottom_up.png';
  static const icClock = 'assets/ic_clock.png';
  static const icTwitter = 'assets/ic_twitter.png';
  static const icMess = 'assets/ic_messenger.png';
  static const icInstagram = 'assets/ic_instagram.png';
  static const icCart = 'assets/ic_cart.png';
  static const imgText = 'assets/ps4_console_white_1.png';
  static const icCartOutline = 'assets/ic_cart_outline.png';
  static const icCartFilled = 'assets/ic_cart_filled.png';
  static const Map = 'assets/lotties/map.json';
  static const imgDefault = 'assets/nhahang1.jpeg';
}
